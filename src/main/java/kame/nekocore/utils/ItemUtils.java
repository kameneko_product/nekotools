package kame.nekocore.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.Nullable;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class ItemUtils {

    public static boolean isEmpty(@Nullable ItemStack item) {
        return item == null || item.getAmount() <= 0 || item.getType() == Material.AIR;
    }

    public static boolean isNotEmpty(@Nullable ItemStack item) {
        return !isEmpty(item);
    }

    public static int countItems(ItemStack[] items, Predicate<ItemStack> filter) {
        return Stream.of(items).filter(ItemUtils::isNotEmpty).filter(filter).mapToInt(ItemStack::getAmount).sum();
    }
}
