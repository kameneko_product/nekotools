package kame.nekocore.utils;

import dev.jorel.commandapi.CommandAPIBukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ProxiedCommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.function.Function;
import java.util.stream.IntStream;

public class StringUtils {

    public static boolean sendMessage(CommandSender sender, String message) {
        return sendMessage(sender, message, true);
    }

    public static boolean sendMessage(CommandSender sender, String message, boolean returnValue) {
        sender.sendMessage(message);
        return returnValue;
    }

    public static boolean hasChars(String string) {
        return string != null && !string.isEmpty();
    }

    public static String replace(String command, CommandSender callee) {
        if (callee instanceof ProxiedCommandSender proxy) {
            return replace(command, proxy.getCallee());
        }
        var args = command.split(" ");
        if (callee instanceof Player target) {
            for (var index : IntStream.range(0, args.length).toArray()) {
                args[index] = player.getOrDefault(args[index], x -> args[index]).apply(target);
            }
        }

        if (callee instanceof LivingEntity target) {
            for (var index : IntStream.range(0, args.length).toArray()) {
                args[index] = living.getOrDefault(args[index], x -> args[index]).apply(target);
            }
        }

        if (callee instanceof Entity target) {
            for (var index : IntStream.range(0, args.length).toArray()) {
                args[index] = entity.getOrDefault(args[index], x -> args[index]).apply(target);
            }
        }
        return String.join(" ", args);
    }


    private record Func<T, U>(Function<? super T, ? extends U> function) {

        public U apply(T player) {
            return function.apply(player);
        }

        public <V> Func<? super T, ? extends V> map(Function<? super U, V> mapFunction) {
            return new Func<>(x -> mapFunction.apply(function.apply(x)));
        }

        static <T, U> Func<? super T, ? extends U> create(Function<? super T, ? extends U> function) {
            return new Func<>(function);
        }
    }

    private static final Map<String, Function<? super Player, String>> player = new HashMap<>(){{
        put("$Name",                 Func.create(Entity::getName)::apply);
        put("$DisplayName",          Func.create(Player::getDisplayName)::apply);
        put("$Locale",               Func.create(Player::getLocale)::apply);
        put("$Level",                Func.create(Player::getLevel).map(Objects::toString)::apply);
        put("$PlayerListName",       Func.create(Player::getPlayerListName)::apply);
        put("$PlayerListHeader",     Func.create(Player::getPlayerListHeader)::apply);
        put("$PlayerListFooter",     Func.create(Player::getPlayerListFooter)::apply);
        put("$PreviousGameMode",     Func.create(Player::getPreviousGameMode).map(Objects::toString)::apply);
        put("$PlayerTimeOffset",     Func.create(Player::getPlayerTimeOffset).map(Objects::toString)::apply);
        put("$ExpCooldown",          Func.create(Player::getExpCooldown).map(Objects::toString)::apply);
        put("$TotalExperience",      Func.create(Player::getTotalExperience).map(Objects::toString)::apply);
        put("$FlySpeed",             Func.create(Player::getFlySpeed).map(Objects::toString)::apply);
        put("$WalkSpeed",            Func.create(Player::getWalkSpeed).map(Objects::toString)::apply);
        put("$HealthScale",          Func.create(Player::getHealthScale).map(Objects::toString)::apply);
        put("$SpectatorTarget",      Func.create(Player::getSpectatorTarget).map(StringUtils::toString)::apply);
        put("$ClientViewDistance",   Func.create(Player::getClientViewDistance).map(Objects::toString)::apply);
        put("$PlayerTime",           Func.create(Player::getPlayerTime).map(Objects::toString)::apply);
        put("$PlayerWeather",        Func.create(Player::getPlayerWeather).map(Objects::toString)::apply);
        put("$Exp",                  Func.create(Player::getExp).map(Objects::toString)::apply);
        put("$Ping",                 Func.create(Player::getPing).map(Objects::toString)::apply);
        put("$Exhaustion",           Func.create(Player::getExhaustion).map(Objects::toString)::apply);
        put("$FoodLevel",            Func.create(Player::getFoodLevel).map(Objects::toString)::apply);
        put("$EnchantmentSeed",      Func.create(Player::getEnchantmentSeed).map(Objects::toString)::apply);
        put("$SleepTicks",           Func.create(Player::getSleepTicks).map(Objects::toString)::apply);
        put("$ExpToLevel",           Func.create(Player::getExpToLevel).map(Objects::toString)::apply);
        put("$AttackCooldown",       Func.create(Player::getAttackCooldown).map(Objects::toString)::apply);
        put("$Saturation",           Func.create(Player::getSaturation).map(Objects::toString)::apply);
        put("$SaturatedRegenRate",   Func.create(Player::getSaturatedRegenRate).map(Objects::toString)::apply);
        put("$UnsaturatedRegenRate", Func.create(Player::getUnsaturatedRegenRate).map(Objects::toString)::apply);
        put("$StarvationRate",       Func.create(Player::getStarvationRate).map(Objects::toString)::apply);
        put("$FirstPlayed",          Func.create(Player::getFirstPlayed).map(Objects::toString)::apply);
        put("$LastPlayed",           Func.create(Player::getLastPlayed).map(Objects::toString)::apply);

        put("$LastDeathLocation",           Func.create(Player::getLastDeathLocation).map(StringUtils::locPositionToString)::apply);
        put("$LastDeathLocation.BlockPos",  Func.create(Player::getLastDeathLocation).map(StringUtils::locBlockPosToString)::apply);
        put("$LastDeathLocation.Rotation",  Func.create(Player::getLastDeathLocation).map(StringUtils::locRotationToString)::apply);
        put("$CompassTarget",               Func.create(Player::getCompassTarget).map(StringUtils::locPositionToString)::apply);
        put("$CompassTarget.BlockPos",      Func.create(Player::getCompassTarget).map(StringUtils::locBlockPosToString)::apply);
        put("$CompassTarget.Rotation",      Func.create(Player::getCompassTarget).map(StringUtils::locRotationToString)::apply);
        put("$BedSpawnLocation",            Func.create(Player::getBedSpawnLocation).map(StringUtils::locPositionToString)::apply);
        put("$BedSpawnLocation.BlockPos",   Func.create(Player::getBedSpawnLocation).map(StringUtils::locBlockPosToString)::apply);
        put("$BedSpawnLocation.Rotation",   Func.create(Player::getBedSpawnLocation).map(StringUtils::locRotationToString)::apply);

        put("$MainHand",                    Func.create(Player::getInventory).map(PlayerInventory::getItemInMainHand).map(StringUtils::itemToString)      ::apply);
        put("$MainHand.Type",               Func.create(Player::getInventory).map(PlayerInventory::getItemInMainHand).map(StringUtils::itemTypeToString)  ::apply);
        put("$MainHand.Name",               Func.create(Player::getInventory).map(PlayerInventory::getItemInMainHand).map(StringUtils::itemNameToString)  ::apply);
        put("$MainHand.Amount",             Func.create(Player::getInventory).map(PlayerInventory::getItemInMainHand).map(StringUtils::itemAmountToString)::apply);
        put("$OffHand",                     Func.create(Player::getInventory).map(PlayerInventory::getItemInOffHand).map(StringUtils::itemToString)      ::apply);
        put("$OffHand.Type",                Func.create(Player::getInventory).map(PlayerInventory::getItemInOffHand).map(StringUtils::itemTypeToString)  ::apply);
        put("$OffHand.Name",                Func.create(Player::getInventory).map(PlayerInventory::getItemInOffHand).map(StringUtils::itemNameToString)  ::apply);
        put("$OffHand.Amount",              Func.create(Player::getInventory).map(PlayerInventory::getItemInOffHand).map(StringUtils::itemAmountToString)::apply);

        put("$ItemInUse",                   Func.create(Player::getItemInUse).map(StringUtils::itemToString)      ::apply);
        put("$ItemInUse.Type",              Func.create(Player::getItemInUse).map(StringUtils::itemTypeToString)  ::apply);
        put("$ItemInUse.Name",              Func.create(Player::getItemInUse).map(StringUtils::itemNameToString)  ::apply);
        put("$ItemInUse.Amount",            Func.create(Player::getItemInUse).map(StringUtils::itemAmountToString)::apply);
        put("$ItemOnCursor",                Func.create(Player::getItemOnCursor).map(StringUtils::itemToString)      ::apply);
        put("$ItemOnCursor.Type",           Func.create(Player::getItemOnCursor).map(StringUtils::itemTypeToString)  ::apply);
        put("$ItemOnCursor.Name",           Func.create(Player::getItemOnCursor).map(StringUtils::itemNameToString)  ::apply);
        put("$ItemOnCursor.Amount",         Func.create(Player::getItemOnCursor).map(StringUtils::itemAmountToString)::apply);

        for (var slot : IntStream.range(0, 40).toArray()) {
            put(String.format("$Inventory.%s",        slot), Func.create(Player::getInventory).map(x -> x.getItem(slot)).map(StringUtils::itemToString)      ::apply);
            put(String.format("$Inventory.%s.Type",   slot), Func.create(Player::getInventory).map(x -> x.getItem(slot)).map(StringUtils::itemTypeToString)  ::apply);
            put(String.format("$Inventory.%s.Name",   slot), Func.create(Player::getInventory).map(x -> x.getItem(slot)).map(StringUtils::itemNameToString)  ::apply);
            put(String.format("$Inventory.%s.Amount", slot), Func.create(Player::getInventory).map(x -> x.getItem(slot)).map(StringUtils::itemAmountToString)::apply);
        }
    }};

    private static final Map<String, Function<LivingEntity, String>> living = new HashMap<>(){{
        put("$CanPickupItems",       Func.create(LivingEntity::getCanPickupItems).map(Objects::toString)::apply);
        put("$EyeHeight",            new Func<LivingEntity, Object>(LivingEntity::getEyeHeight).map(Objects::toString)::apply);
        put("$RemainingAir",         Func.create(LivingEntity::getRemainingAir).map(Objects::toString)::apply);
        put("$MaximumAir",           Func.create(LivingEntity::getMaximumAir).map(Objects::toString)::apply);
        put("$ArrowCooldown",        Func.create(LivingEntity::getArrowCooldown).map(Objects::toString)::apply);
        put("$ArrowsInBody",         Func.create(LivingEntity::getArrowsInBody).map(Objects::toString)::apply);
        put("$MaximumNoDamageTicks", Func.create(LivingEntity::getMaximumNoDamageTicks).map(Objects::toString)::apply);
        put("$LastDamage",           Func.create(LivingEntity::getLastDamage).map(Objects::toString)::apply);
        put("$NoDamageTicks",        Func.create(LivingEntity::getNoDamageTicks).map(Objects::toString)::apply);
        put("$NoActionTicks",        Func.create(LivingEntity::getNoActionTicks).map(Objects::toString)::apply);
        put("$RemoveWhenFarAway",    Func.create(LivingEntity::getRemoveWhenFarAway).map(Objects::toString)::apply);
        put("$Killer",               Func.create(LivingEntity::getKiller).map(StringUtils::toString)::apply);
        put("$AbsorptionAmount",     Func.create(LivingEntity::getAbsorptionAmount).map(Objects::toString)::apply);
        put("$Health",               Func.create(LivingEntity::getHealth).map(Objects::toString)::apply);

        put("$EyeLocation",                Func.create(LivingEntity::getEyeLocation).map(StringUtils::locPositionToString)::apply);
        put("$EyeLocation.BlockPos",       Func.create(LivingEntity::getEyeLocation).map(StringUtils::locBlockPosToString)::apply);
        put("$EyeLocation.Rotation",       Func.create(LivingEntity::getEyeLocation).map(StringUtils::locRotationToString)::apply);

        put("$Equipment.Helmet",            Func.create(LivingEntity::getEquipment).map(EntityEquipment::getHelmet).map(StringUtils::itemToString)      ::apply);
        put("$Equipment.Helmet.Type",       Func.create(LivingEntity::getEquipment).map(EntityEquipment::getHelmet).map(StringUtils::itemTypeToString)  ::apply);
        put("$Equipment.Helmet.Name",       Func.create(LivingEntity::getEquipment).map(EntityEquipment::getHelmet).map(StringUtils::itemNameToString)  ::apply);
        put("$Equipment.Helmet.Amount",     Func.create(LivingEntity::getEquipment).map(EntityEquipment::getHelmet).map(StringUtils::itemAmountToString)::apply);
        put("$Equipment.Chestplate",        Func.create(LivingEntity::getEquipment).map(EntityEquipment::getChestplate).map(StringUtils::itemToString)      ::apply);
        put("$Equipment.Chestplate.Type",   Func.create(LivingEntity::getEquipment).map(EntityEquipment::getChestplate).map(StringUtils::itemTypeToString)  ::apply);
        put("$Equipment.Chestplate.Name",   Func.create(LivingEntity::getEquipment).map(EntityEquipment::getChestplate).map(StringUtils::itemNameToString)  ::apply);
        put("$Equipment.Chestplate.Amount", Func.create(LivingEntity::getEquipment).map(EntityEquipment::getChestplate).map(StringUtils::itemAmountToString)::apply);
        put("$Equipment.Leggings",          Func.create(LivingEntity::getEquipment).map(EntityEquipment::getLeggings).map(StringUtils::itemToString)      ::apply);
        put("$Equipment.Leggings.Type",     Func.create(LivingEntity::getEquipment).map(EntityEquipment::getLeggings).map(StringUtils::itemTypeToString)  ::apply);
        put("$Equipment.Leggings.Name",     Func.create(LivingEntity::getEquipment).map(EntityEquipment::getLeggings).map(StringUtils::itemNameToString)  ::apply);
        put("$Equipment.Leggings.Amount",   Func.create(LivingEntity::getEquipment).map(EntityEquipment::getLeggings).map(StringUtils::itemAmountToString)::apply);
        put("$Equipment.Boots",             Func.create(LivingEntity::getEquipment).map(EntityEquipment::getBoots).map(StringUtils::itemToString)      ::apply);
        put("$Equipment.Boots.Type",        Func.create(LivingEntity::getEquipment).map(EntityEquipment::getBoots).map(StringUtils::itemTypeToString)  ::apply);
        put("$Equipment.Boots.Name",        Func.create(LivingEntity::getEquipment).map(EntityEquipment::getBoots).map(StringUtils::itemNameToString)  ::apply);
        put("$Equipment.Boots.Amount",      Func.create(LivingEntity::getEquipment).map(EntityEquipment::getBoots).map(StringUtils::itemAmountToString)::apply);

    }};

    private static final Map<String, Function<? super Entity, String>> entity = new HashMap<>(){{
        put("$Type",                 Func.create(Entity::getType).map(Objects::toString)::apply);
        put("$Velocity",             Func.create(Entity::getVelocity).map(StringUtils::toString)::apply);
        put("$EntityId",             Func.create(Entity::getEntityId).map(Objects::toString)::apply);
        put("$FireTicks",            Func.create(Entity::getFireTicks).map(Objects::toString)::apply);
        put("$MaxFireTicks",         Func.create(Entity::getMaxFireTicks).map(Objects::toString)::apply);
        put("$FreezeTicks",          Func.create(Entity::getFreezeTicks).map(Objects::toString)::apply);
        put("$MaxFreezeTicks",       Func.create(Entity::getMaxFreezeTicks).map(Objects::toString)::apply);
        put("$FallDistance",         Func.create(Entity::getFallDistance).map(Objects::toString)::apply);
        put("$LastDamageCause",      Func.create(Entity::getLastDamageCause).map(Objects::toString)::apply);
        put("$UUID",                 Func.create(Entity::getUniqueId).map(Objects::toString)::apply);
        put("$TicksLived",           Func.create(Entity::getTicksLived).map(Objects::toString)::apply);
        put("$PortalCooldown",       Func.create(Entity::getPortalCooldown).map(Objects::toString)::apply);
        put("$World",                Func.create(Entity::getWorld).map(World::getName)::apply);
        put("$WorldId",              Func.create(Entity::getWorld).map(World::getUID).map(Objects::toString)::apply);
        put("$Height",               Func.create(Entity::getHeight).map(Objects::toString)::apply);
        put("$Width",                Func.create(Entity::getWidth).map(Objects::toString)::apply);
        put("$Facing",               Func.create(Entity::getFacing).map(Objects::toString)::apply);
        put("$Pose",                 Func.create(Entity::getPose).map(Objects::toString)::apply);
        put("$CustomName",           Func.create(Entity::getCustomName)::apply);

        put("$Location",             new Func<Entity, Location>(Entity::getLocation).map(StringUtils::locPositionToString)::apply);
        put("$Location.BlockPos",    new Func<Entity, Location>(Entity::getLocation).map(StringUtils::locBlockPosToString)::apply);
        put("$Location.Rotation",    new Func<Entity, Location>(Entity::getLocation).map(StringUtils::locRotationToString)::apply);
    }};

    public static Map<String, Function<? super Player, String>> getReplaceTags(Player target) {
        Function<? super LivingEntity, ?> a = LivingEntity::getEyeHeight;
        return new HashMap<>() {{
            putAll(player);
            putAll(living);
            putAll(entity);
            for (var objective : target.getScoreboard().getObjectives()) {
                put(String.format("$Scoreboard.%s", objective.getName()), new Func<Player, String>(x -> getScore(x, objective.getName()))::apply);
            }
        }

            private String getScore(Player player, String name) {
                return Optional.ofNullable(player.getScoreboard().getObjective(name)).map(x -> "" + x.getScore(player.getName()).getScore()).orElse("");
            }

        };
    }

    private static String toString(Vector vec) {
        return "%s %s %s".formatted(vec.getX(), vec.getY(), vec.getZ());
    }

    private static String toString(Entity entity) {
        return "%s".formatted(Optional.ofNullable(entity).map(Entity::getUniqueId).map(Objects::toString).orElse("null"));
    }

    private static String locPositionToString(Location location) {
        var loc = Optional.ofNullable(location).orElse(new Location(null, 0, 0, 0));
        return "%s %s %s".formatted(loc.getX(), loc.getY(), loc.getZ());
    }

    private static String locBlockPosToString(Location location) {
        var loc = Optional.ofNullable(location).orElse(new Location(null, 0, 0, 0));
        return "%s %s %s".formatted(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
    }

    private static String locRotationToString(Location location) {
        var loc = Optional.ofNullable(location).orElse(new Location(null, 0, 0, 0));
        return "%s %s".formatted(loc.getYaw(), loc.getPitch());
    }

    private static String itemToString(ItemStack item) {
        return CommandAPIBukkit.get().convert(Optional.ofNullable(item).orElse(new ItemStack(Material.AIR, 0)));
    }

    private static String itemTypeToString(ItemStack item) {
        return Optional.ofNullable(item).orElse(new ItemStack(Material.AIR, 0)).getType().getKey().toString();
    }

    private static String itemNameToString(ItemStack item) {
        return Optional.ofNullable(item).map(ItemStack::getItemMeta).map(ItemMeta::getDisplayName).orElse("");
    }

    private static String itemAmountToString(ItemStack item) {
        return "%s".formatted(Optional.ofNullable(item).orElse(new ItemStack(Material.AIR, 0)).getAmount());
    }

    public static String[] split(String text, String splitter) {
        var list = new ArrayList<String>();
        var start = 0;
        var end = 0;
        var split = splitter.toCharArray();
        var chars = text.toCharArray();
        var quoted = false;
        for (var cursor = 0; cursor < chars.length - split.length; cursor++) {
            if (chars[cursor] == '"') quoted ^= true;
            if (quoted) continue;
            if (Arrays.equals(split, 0, split.length, chars, cursor, cursor + split.length)) {
                list.add(text.substring(start, cursor));
                start = cursor + split.length;
            }
        }
        list.add(text.substring(start));
        return list.toArray(String[]::new);
    }
}
