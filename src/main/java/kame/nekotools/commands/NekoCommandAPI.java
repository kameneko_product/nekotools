package kame.nekotools.commands;

import dev.jorel.commandapi.CommandAPIBukkit;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ProxiedCommandSender;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class NekoCommandAPI {

    private static final String VERSION = Bukkit.getServer().getClass().getPackage().getName();

    private static final Class<?> Class_ProxiedNativeCommandSender = ReflectionTools.getClass(VERSION + ".command.ProxiedNativeCommandSender");

    private static final Class<?> Class_CommandListenerWrapper = ReflectionTools.getClass("net.minecraft.commands.CommandListenerWrapper");

    private static final Constructor<?> Constructor_ProxyCommandSender = ReflectionTools.getConstructor(Class_ProxiedNativeCommandSender, Class_CommandListenerWrapper, CommandSender.class, CommandSender.class);

    public static ProxiedCommandSender wrapForProxySender(CommandSender sender, CommandSender caller, CommandSender callee) {
        var bukkitSender = CommandAPIBukkit.get().wrapCommandSender(sender);
        var wrapper = CommandAPIBukkit.get().getBrigadierSourceFromCommandSender(bukkitSender);
        try {
            return (ProxiedCommandSender)Constructor_ProxyCommandSender.newInstance(wrapper, caller, callee);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static Object wrapForBrigadierListener(CommandSender sender) {
        var bukkitSender = CommandAPIBukkit.get().wrapCommandSender(sender);
        return CommandAPIBukkit.get().getBrigadierSourceFromCommandSender(bukkitSender);
    }

    private static class ReflectionTools {
        public static Class<?> getClass(String name) {
            try {
                return Class.forName(name);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        public static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes) {
            try {
                return clazz.getConstructor(parameterTypes);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
