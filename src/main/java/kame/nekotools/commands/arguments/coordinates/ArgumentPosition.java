package kame.nekotools.commands.arguments.coordinates;

public record ArgumentPosition(boolean isRelative, double value) {

    public ArgumentPosition() {
        this(false, 0);
    }
}