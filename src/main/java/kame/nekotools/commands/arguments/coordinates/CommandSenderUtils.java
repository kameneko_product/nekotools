package kame.nekotools.commands.arguments.coordinates;

import dev.jorel.commandapi.wrappers.NativeProxyCommandSender;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.ProxiedCommandSender;
import org.bukkit.entity.Entity;

public class CommandSenderUtils {

    public static Location getLocation(CommandSender sender) {
        return sender instanceof Entity send ? getLocation(send)
                : sender instanceof BlockCommandSender send ? getLocation(send)
                : sender instanceof ConsoleCommandSender send ? getLocation(send)
                : sender instanceof ProxiedCommandSender send ? getLocation(send)
                : throwLocationException();
    }

    public static Location getLocation(ProxiedCommandSender sender) {
        return sender instanceof NativeProxyCommandSender nativeSender ? getLocation(nativeSender) :
                getLocation(sender.getCallee());
    }

    public static Location getLocation(ConsoleCommandSender sender) {
        throw new UnsupportedOperationException(sender.getClass() + " is not supported getLocation method");
    }

    public static Location getLocation(BlockCommandSender sender) {
        return sender.getBlock().getLocation();
    }

    public static Location getLocation(Entity sender) {
        return sender.getLocation();
    }

    public static Location getLocation(NativeProxyCommandSender sender) {
        return sender.getLocation();
    }

    private static Location throwLocationException() {
        throw new UnsupportedOperationException("Unknown CommandSender");
    }
}
