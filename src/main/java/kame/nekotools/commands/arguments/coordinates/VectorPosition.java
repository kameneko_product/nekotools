package kame.nekotools.commands.arguments.coordinates;

import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.util.Vector;

public record VectorPosition(boolean isLocal, ArgumentPosition getX, ArgumentPosition getY, ArgumentPosition getZ) {
    public static final VectorPosition ZERO = new VectorPosition();

    public VectorPosition() {
        this(false, new ArgumentPosition(), new ArgumentPosition(), new ArgumentPosition());
    }

    public Location getLocation(CommandSender sender) {
        var loc = CommandSenderUtils.getLocation(sender);
        if (isLocal) {
            var vec = new Vector(getX.value(), getY.value(), getZ.value())
                    .rotateAroundX(Math.toRadians(loc.getYaw()))
                    .rotateAroundY(Math.toRadians(-loc.getPitch()));
            return loc.add(vec);
        } else {
            loc.setX(getX.value() + (getX.isRelative() ? loc.getX() : 0));
            loc.setY(getY.value() + (getY.isRelative() ? loc.getY() : 0));
            loc.setZ(getZ.value() + (getZ.isRelative() ? loc.getZ() : 0));
            return loc;
        }
    }

    public Vector getVector(CommandSender sender) {
        var vec = new Vector(getX.value(), getY.value(), getZ.value());
        if (isLocal) {
            var loc = CommandSenderUtils.getLocation(sender);
            vec.rotateAroundX(Math.toRadians(loc.getYaw()))
                    .rotateAroundY(Math.toRadians(-loc.getPitch()));
        }
        return vec;
    }

    @Override
    public String toString() {
        return isLocal ? "^%f ^%f ^%f".formatted(getX.value(), getY.value(), getZ.value())
                : "%s%f %s%f %s%f".formatted(
                getX.isRelative() ? "~" : "", getX.value(),
                getY.isRelative() ? "~" : "", getY.value(),
                getZ.isRelative() ? "~" : "", getZ.value());
    }
}