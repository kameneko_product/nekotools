package kame.nekotools.commands.effect;

import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.wrappers.ParticleData;
import kame.nekocore.math.Bezier3d;
import kame.nekocore.math.BezierPointIterator;
import kame.nekocore.math.Line3d;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.arguments.VectorPositionArgument;
import kame.nekotools.commands.arguments.coordinates.VectorPosition;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.util.Vector;

public class AsyncParticleBezierCommand {

    private static final String description =
            "§6Description:§r 非同期処理でパーティクルエフェクトを発生させます。";

    static {
        Register.create("asyncparticle", description, "nekotools.command.asyncparticle")
                .withArguments(new LiteralArgument("bezier"))
                .withArguments(new ParticleArgument("name"))
                .withArguments(new LocationArgument("p1"))
                .withArguments(new LocationArgument("p2"))
                .withArguments(new LocationArgument("p3"))
                .withArguments(new LocationArgument("p4"))
                .withArguments(new VectorPositionArgument("motion"))
                .withArguments(new DoubleArgument("extra"))
                .withArguments(new IntegerArgument("amount", 0))
                .withArguments(new IntegerArgument("period"))
                .withArguments(new IntegerArgument("loop_count"))
                .withArguments(new DoubleArgument("factor", 0))
                .withArguments(new MultiLiteralArgument("force", "normal"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("target"))
                .executes(AsyncParticleBezierCommand::bezier)
                .register();

        Register.create("asyncparticle", description, "nekotools.command.asyncparticle")
                .withArguments(new LiteralArgument("bezier"))
                .withArguments(new ParticleArgument("name"))
                .withArguments(new LocationArgument("p1"))
                .withArguments(new LocationArgument("p2"))
                .withArguments(new LocationArgument("p3"))
                .withArguments(new LocationArgument("p4"))
                .withArguments(new VectorPositionArgument("m1"))
                .withArguments(new VectorPositionArgument("m2"))
                .withArguments(new VectorPositionArgument("m3"))
                .withArguments(new VectorPositionArgument("m4"))
                .withArguments(new DoubleArgument("extra"))
                .withArguments(new IntegerArgument("amount", 0))
                .withArguments(new IntegerArgument("period"))
                .withArguments(new IntegerArgument("loop_count"))
                .withArguments(new DoubleArgument("factor", 0))
                .withArguments(new MultiLiteralArgument("force", "normal"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("target"))
                .executes(AsyncParticleBezierCommand::bezier)
                .register();
    }

    private static int bezier(CommandSender sender, CommandArgs args) {
        //asp type data? bezier x1 y1 z1 x2 y2 z2 x3 y3 z3 x4 y4 z4 dx1 dy1 dz1 extra count period loop_count force|normal entity
        var particle = args.getOrSupply("name", CommandArgs.thrown(ParticleData.class));
        var p1 = args.getOrSupply("p1", CommandArgs.thrown(Location.class));
        var p2 = args.getOrSupply("p2", CommandArgs.thrown(Location.class));
        var p3 = args.getOrSupply("p3", CommandArgs.thrown(Location.class));
        var p4 = args.getOrSupply("p4", CommandArgs.thrown(Location.class));
        var m = args.getOrSupply("motion", CommandArgs.value(VectorPosition.ZERO));
        var m1 = args.getOrSupply("m1", CommandArgs.value(m));
        var m2 = args.getOrSupply("m2", CommandArgs.value(m));
        var m3 = args.getOrSupply("m3", CommandArgs.value(m));
        var m4 = args.getOrSupply("m4", CommandArgs.value(m));
        var extra = args.getOrSupply("extra", CommandArgs.thrown(Double.class));
        int count = args.getOrSupply("amount", CommandArgs.thrown(Integer.class));
        int period = args.getOrSupply("period", CommandArgs.thrown(Integer.class));
        int loop_count = args.getOrSupply("loop_count", CommandArgs.thrown(Integer.class));
        double factor = args.getOrSupply("factor", CommandArgs.thrown(Double.class));
        var players = args.getOrSupply("target", CommandArgs::throwPlayers);

        var v1 = new Vector(m1.getX().value(), m1.getY().value(), m1.getZ().value());
        var v2 = new Vector(m2.getX().value(), m2.getY().value(), m2.getZ().value());
        var v3 = new Vector(m3.getX().value(), m3.getY().value(), m3.getZ().value());
        var v4 = new Vector(m4.getX().value(), m4.getY().value(), m4.getZ().value());
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getPlugin(NekoTools.class), () -> {
            var lBezier = new Bezier3d(new Line3d(p1, p2), new Line3d(p3, p4));
            var mBezier = new Bezier3d(new Line3d(v1, v2), new Line3d(v3, v4));
            var lIterator = new BezierPointIterator(lBezier, loop_count, factor);
            var mIterator = new BezierPointIterator(mBezier, loop_count, factor);
            while (lIterator.hasNext()) {
                var pos = lIterator.next();
                var mot = mIterator.next();
                players.forEach(x -> x.spawnParticle(particle.particle(),
                        pos.x(), pos.y(), pos.z(), count,
                        mot.x(), mot.y(), mot.z(),
                        extra,
                        particle.data()));
                sleep(period);
            }
        });
        return 0;
    }

    private static void sleep(int period) {
        if (period > 0) {
            try {
                Thread.sleep(period);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
