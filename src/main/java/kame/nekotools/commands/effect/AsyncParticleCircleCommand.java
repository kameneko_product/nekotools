package kame.nekotools.commands.effect;

import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.wrappers.FloatRange;
import dev.jorel.commandapi.wrappers.ParticleData;
import dev.jorel.commandapi.wrappers.Rotation;
import kame.nekocore.math.Circle3d;
import kame.nekocore.math.Point3d;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.arguments.VectorPositionArgument;
import kame.nekotools.commands.arguments.coordinates.VectorPosition;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.util.Vector;

public class AsyncParticleCircleCommand {

    private static final String description =
            "§6Description:§r 非同期処理でパーティクルエフェクトを発生させます。";

    static {
        Register.create("asyncparticle", description, "nekotools.command.asyncparticle")
                .withArguments(new LiteralArgument("circle"))
                .withArguments(new ParticleArgument("name"))
                .withArguments(new LocationArgument("center"))
                .withArguments(new RotationArgument("rotate"))
                .withArguments(new DoubleArgument("radius"))
                .withArguments(new FloatRangeArgument("range"))
                .withArguments(new VectorPositionArgument("motion"))
                .withArguments(new DoubleArgument("extra"))
                .withArguments(new IntegerArgument("amount", 0))
                .withArguments(new IntegerArgument("period"))
                .withArguments(new IntegerArgument("loop_count"))
                .withArguments(new MultiLiteralArgument("force", "normal"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("target"))
                .executes(AsyncParticleCircleCommand::circle)
                .register();
    }

    private static int circle(CommandSender sender, CommandArgs args) {
        var particle = args.getOrSupply("name", CommandArgs.thrown(ParticleData.class));
        var center = args.getOrSupply("center", CommandArgs.thrown(Location.class));
        var rotation = args.getOrSupply("rotate", CommandArgs.thrown(Rotation.class));
        var radius = args.getOrSupply("radius", CommandArgs.thrown(Double.class));
        var range = args.getOrSupply("range", CommandArgs.thrown(FloatRange.class));
        var motion = args.getOrSupply("motion", CommandArgs.thrown(VectorPosition.class));
        var extra = args.getOrSupply("extra", CommandArgs.thrown(Double.class));
        int count = args.getOrSupply("amount", CommandArgs.thrown(Integer.class));
        int period = args.getOrSupply("period", CommandArgs.thrown(Integer.class));
        int loop_count = args.getOrSupply("loop_count", CommandArgs.thrown(Integer.class));
        var players = args.getOrSupply("target", CommandArgs::throwPlayers);

        var vector = new Vector(motion.getX().value(), motion.getY().value(), motion.getZ().value());
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getPlugin(NekoTools.class), () -> {
            var circle = new Circle3d(new Point3d(center), radius, rotation.getPitch(), rotation.getYaw());
            var length = range.getUpperBound() - range.getLowerBound();
            for (var i = 0; i < loop_count; i++) {
                var pos = circle.getPoint(((double)i / loop_count) * length + range.getLowerBound());
                players.forEach(x -> x.spawnParticle(particle.particle(),
                        pos.x(), pos.y(), pos.z(), count,
                        vector.getX(), vector.getY(), vector.getZ(),
                        extra,
                        particle.data()));
                sleep(period);
            }
        });
        return 0;
    }

    private static void sleep(int period) {
        if (period > 0) {
            try {
                Thread.sleep(period);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
