package kame.nekotools.commands.entity;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.wrappers.Rotation;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class RespawnCommand implements Listener {

    private static final String description =
            "§6Description:§r 対象のプレイヤーのテレポート履歴からテレポートさせます。";

    private static final Map<UUID, Location> respawnLocations = new HashMap<>();

    static {
        Register.create("respawn", description, "nekotools.command.respawn")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .executes(RespawnCommand::respawn)
                .register();

        Register.create("respawn", description, "nekotools.command.respawn")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LocationArgument("location"))
                .withOptionalArguments(new RotationArgument("rotation"))
                .withOptionalArguments(new WorldArgument("world"))
                .executes(RespawnCommand::respawnLocation)
                .register();

        Register.create("respawn", description, "nekotools.command.respawn")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LiteralArgument("set"))
                .withArguments(new LocationArgument("location"))
                .withOptionalArguments(new RotationArgument("rotation"))
                .withOptionalArguments(new WorldArgument("world"))
                .executes(RespawnCommand::setRespawn)
                .register();

        NekoTools.getInstance().addRunOnEnablingQueue(RespawnCommand::initialize);
    }

    private static void initialize() {
        Bukkit.getPluginManager().registerEvents(new RespawnCommand(), NekoTools.getInstance());
    }

    private static void setRespawn(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        for (var target : args.getOrSupply("targets", CommandArgs::throwPlayers)) {
            if (!target.isDead()) continue;
            var loc = args.getOrSupply("location", CommandArgs.thrown(Location.class));
            if (args.get("rotation") instanceof Rotation rot) {
                loc.setPitch(rot.getPitch());
                loc.setYaw(rot.getYaw());
            }
            if (args.get("world") instanceof World world) {
                loc.setWorld(world);
            }
            respawnLocations.put(target.getUniqueId(), loc);
        }
        if (args.getOrSupply("targets", CommandArgs::throwPlayers).isEmpty()) {
            throw CommandAPI.failWithString("[NekoTools] プレイヤーが見つかりませんでした。");
        }
    }

    private static void respawn(CommandSender sender, CommandArgs args) {
        for (var target : args.getOrSupply("targets", CommandArgs::throwPlayers)) {
            target.spigot().respawn();
        }
    }

    private static void respawnLocation(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        setRespawn(sender, args);
        respawn(sender, args);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onRespawn(PlayerRespawnEvent event) {
        var location = respawnLocations.remove(event.getPlayer().getUniqueId());
        if (location != null) event.setRespawnLocation(location);
    }
}
