package kame.nekotools.commands.entity;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekotools.commands.arguments.MaterialArgument;
import kame.nekotools.commands.arguments.VectorArgument;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SetStateCommand {

    private static final String description =
            "§6Description:§r 指定したプレイヤーのステータスを設定します。";

    static {
        methods:
        for (var method : Player.class.getMethods()) {
            Bukkit.broadcastMessage(method.getName());
            if (!method.getName().startsWith("set")) continue;
            var command = Register.create("setstate", description, "nekotools.command.setstate")
                    .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                    .withArguments(new LiteralArgument(method.getName().substring(3)));
            var params = new ArrayList<String>();
            var parameters = Arrays.stream(method.getParameters())
                    .map(x -> x.getName().replace("arg", x.getType().getSimpleName() + "_"))
                    .toList();

            for (var param : method.getParameters()) {
                var name = param.getName().replace("arg", param.getType().getSimpleName() + "_");
                var argument = getArgument(name, param.getType());
                if (argument == null) continue methods;
                command.withArguments(argument);
                params.add(name);
            }
            command.executes((CommandSender x, CommandArgs y) -> setState(x, y, method, params));
            command.register();
        }
    }

    private static Argument<?> getArgument(String name, Class<?> type) {
        if (type == null) return null;
        else if (type.equals(Material.class))  /**/ return new MaterialArgument(name);
        else if (type.equals(boolean.class))   /**/ return new BooleanArgument(name);
        else if (type.equals(byte.class))      /**/ return new IntegerArgument(name, Byte.MIN_VALUE, Byte.MAX_VALUE);
        else if (type.equals(short.class))     /**/ return new IntegerArgument(name, Short.MIN_VALUE, Short.MAX_VALUE);
        else if (type.equals(int.class))       /**/ return new IntegerArgument(name);
        else if (type.equals(long.class))      /**/ return new LongArgument(name);
        else if (type.equals(float.class))     /**/ return new FloatArgument(name);
        else if (type.equals(double.class))    /**/ return new DoubleArgument(name);
        else if (type.equals(String.class))    /**/ return new StringArgument(name);
        else if (type.equals(Location.class))  /**/ return new LocationArgument(name);
        else if (type.equals(Vector.class))    /**/ return new VectorArgument(name);
        else if (type.equals(ItemStack.class)) /**/ return new ItemStackArgument(name);
        else if (type.equals(Entity.class))    /**/ return new EntitySelectorArgument.OneEntity(name);
        return null;
    }

    private static void setState(CommandSender sender, CommandArgs args, Method method, List<String> params) throws WrapperCommandSyntaxException {
        var targets = args.getOrSupply("targets", CommandArgs::throwPlayers);
        for (var target : targets) {
            try {
                method.invoke(target, params.stream().map(args::get).toArray());
            } catch (Exception e) {
                sender.sendMessage("§c[NekoTools] §r" + target.getName() + "のステータスの設定に失敗しました。");
                e.printStackTrace();
            }
        }
        if (targets.isEmpty()) throw CommandAPI.failWithString("[NekoTools] プレイヤーが見つかりませんでした。");
        var methodName = method.getName().substring(3);
        var values = String.join(", ", params.stream().map(args::get).map(Object::toString).toList());
        if (targets.size() == 1) {
            var target = targets.iterator().next().getName();
            var message = String.format("%sの%sを[%s]に設定しました。", target, methodName, values);
            sender.sendMessage(String.format("§b[NekoTools] §r%s", message));
        } else {
            var count = targets.size();
            var message = String.format("%s人の%sを[%s]に設定しました。", count, methodName, values);
            sender.sendMessage(String.format("§b[NekoTools] §r%s", message));
        }
    }
}