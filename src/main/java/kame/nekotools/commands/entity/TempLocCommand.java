package kame.nekotools.commands.entity;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.MultiLiteralArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import kame.nekotools.utils.WorldUtils;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Stream;

public class TempLocCommand implements Listener {

    private static final String description =
            "§6Description:§r 指定したテレポートの履歴へ戻る機能を提供します。";

    private static final Map<UUID, List<History>> historyStore = new HashMap<>();

    private static final Set<UUID> skipping = new HashSet<>();

    static {
        var causes = PlayerTeleportEvent.TeleportCause.values();
        var teleports = new ArrayList<>(Stream.of(causes).map(Object::toString).toList());
        teleports.add("DEATH");
        Register.create("temploc", description, "nekotools.command.temploc")
                .withArguments(new EntitySelectorArgument.OnePlayer("targets"))
                .executesPlayer(TempLocCommand::queryHistory)
                .register();

        Register.create("temploc", description, "nekotools.command.temploc")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new IntegerArgument("index", 0))
                .withArguments(new MultiLiteralArgument("from_to", "from", "to"))
                .withOptionalArguments(new MultiLiteralArgument("cause", teleports.toArray(String[]::new)).setListed(true))
                .executes(TempLocCommand::moveBack)
                .register();

        NekoTools.getInstance().addRunOnEnablingQueue(TempLocCommand::initialize);
    }

    private static void initialize() {
        Bukkit.getPluginManager().registerEvents(new TempLocCommand(), NekoTools.getInstance());
    }

    private static void queryHistory(Player sender, CommandArgs args) {
        var target = args.getOrSupply("targets", CommandArgs.thrown(Player.class));
        var histories = historyStore.getOrDefault(target.getUniqueId(), new ArrayList<>());
        sender.sendMessage("§a[NekoTools]§r " + target.getName() + "のテレポート履歴");
        var longFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        var shortFormat = DateTimeFormatter.ofPattern("MM/dd HH:mm");
        var comFormat = "/tpto %s %s %s %s %s %s";

        var index = 0;
        for (var history : histories) {
            sender.spigot().sendMessage(new TextComponent("§2§l" + index++ + "§r: "), history.getTextComponent(sender));
        }
    }

    private static void moveBack(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        for (var target : args.getOrSupply("targets", CommandArgs::throwPlayers)) {
            var histories = historyStore.getOrDefault(target.getUniqueId(), new ArrayList<>());
            var cause = args.getOrSupply("cause", CommandArgs.empty(TeleportCause.class));
            var filtered = histories.stream().filter(x -> cause.isEmpty() || cause.contains(x.cause)).toList();
            try {
                var history = filtered.get(args.getOrThrow("index", Integer.class));
                var location = args.getOrThrow("from_to", String.class).equals("from") ? history.from : history.to;
                skipping.add(target.getUniqueId());
                target.teleport(location, PlayerTeleportEvent.TeleportCause.PLUGIN);
                var message = String.format("%sを%sにテレポートさせました。", target.getName(), location);
                sender.sendMessage(String.format("§a[NekoTools]§r %s", message));
            } catch (IndexOutOfBoundsException ignored) {
                sender.sendMessage(String.format("§c[NekoTools] indexの値が参照出来る履歴数(%s)を超えています。", histories.size()));
            }
        }
        if (args.getOrSupply("targets", CommandArgs::throwPlayers).isEmpty()) {
            throw CommandAPI.failWithString("[NekoTools] プレイヤーが見つかりませんでした。");
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onTeleport(PlayerTeleportEvent event) {
        if (skipping.remove(event.getPlayer().getUniqueId())) return;
        var histories = historyStore.getOrDefault(event.getPlayer().getUniqueId(), new ArrayList<>());
        histories.add(new History(event.getFrom(), event.getTo(), LocalDateTime.now(), TeleportCause.fromPlayer(event.getCause())));
        while (histories.size() > 100) histories.remove(0);
        historyStore.put(event.getPlayer().getUniqueId(), histories);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onRespawn(PlayerRespawnEvent event) {
        if (skipping.remove(event.getPlayer().getUniqueId())) return;
        var histories = historyStore.getOrDefault(event.getPlayer().getUniqueId(), new ArrayList<>());
        var cause = switch (event.getRespawnReason()) {
            case DEATH -> TeleportCause.RESPAWN;
            case END_PORTAL -> TeleportCause.END_PORTAL;
            case PLUGIN -> TeleportCause.PLUGIN;
        };
        histories.add(new History(event.getRespawnLocation(), event.getPlayer().getLocation(), LocalDateTime.now(), cause));
        while (histories.size() > 100) histories.remove(0);
        historyStore.put(event.getPlayer().getUniqueId(), histories);
    }

    private record History(Location from, Location to, LocalDateTime date, TeleportCause cause) {
        private static final DateTimeFormatter longFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        private static final DateTimeFormatter shortFormat = DateTimeFormatter.ofPattern("MM/dd HH:mm");
        private static final String comFormat = "/tpto %s %s %s %s %s %s";

        public TextComponent getTextComponent(Player sender) {
            var component = new TextComponent("§b§l" + date.format(shortFormat) + "§r " + cause);
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(date.format(longFormat))));
            component.addExtra("                                     \n    ");
            component.addExtra(getLocationComponent(sender, from));
            component.addExtra(" §6§l->§r ");
            component.addExtra(getLocationComponent(sender, to));
            return component;
        }

        private static TextComponent getLocationComponent(Player sender, Location loc) {
            var worldName = WorldUtils.getDimensionName(loc.getWorld());
            var component = new TextComponent(worldName);
            var locStr = String.format("%s %s %s %s %s", loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
            var command = String.format("/tpto %s %s %s", sender.getName(), locStr, component.getText());
            component.addExtra(String.format("[%s, %s, %s]", loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()));
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("§nクリックで移動")));
            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
            component.setUnderlined(true);
            return component;
        }
    }

    private enum TeleportCause {
        ENDER_PEARL(ChatColor.DARK_GREEN),
        COMMAND(ChatColor.GOLD),
        PLUGIN(ChatColor.WHITE),
        NETHER_PORTAL(ChatColor.DARK_PURPLE),
        END_PORTAL(ChatColor.DARK_GRAY),
        SPECTATE(ChatColor.GRAY),
        END_GATEWAY(ChatColor.DARK_GRAY),
        CHORUS_FRUIT(ChatColor.DARK_GRAY),
        DISMOUNT(ChatColor.GRAY),
        EXIT_BED(ChatColor.RED),
        RESPAWN(ChatColor.DARK_RED),
        UNKNOWN(ChatColor.YELLOW),
        ;

        private final ChatColor color;

        TeleportCause(ChatColor color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "Cause: " + color + this.name() + ChatColor.RESET;
        }

        public static TeleportCause fromPlayer(PlayerTeleportEvent.TeleportCause cause) {
            return TeleportCause.valueOf(cause.toString());
        }
    }
}
