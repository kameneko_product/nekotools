package kame.nekotools.commands.entity;

import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.SuggestionInfo;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.LocationArgument;
import dev.jorel.commandapi.arguments.RotationArgument;
import dev.jorel.commandapi.arguments.WorldArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.wrappers.Rotation;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class TpToCommand {

    private static final String description =
            "§6Description:§r 対象のプレイヤーを指定位置にテレポートさせます。";

    static {
        Register.create("tpto", description, "nekotools.command.temploc")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LocationArgument("location"))
                .withOptionalArguments(new RotationArgument("rotation"))
                .withOptionalArguments(new WorldArgument("world"))
                .executes(TpToCommand::teleport)
                .register();

        Register.create("tpto", description, "nekotools.command.temploc")
                .withArguments(new LocationArgument("location"))
                .withOptionalArguments(new RotationArgument("rotation"))
                .withOptionalArguments(new WorldArgument("world"))
                .executesPlayer(TpToCommand::teleport)
                .register();

        Register.create("tpto", description, "nekotools.command.temploc")
                .withArguments(new LocationArgument("location"))
                .withOptionalArguments(new RotationArgument("rotation"))
                .withOptionalArguments(new WorldArgument("world")
                        .replaceSuggestions(TpToCommand::suggest))
                .executesPlayer(TpToCommand::teleport)
                .register();
    }

    private static CompletableFuture<Suggestions> suggest(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) {
        for (var world : Bukkit.getWorlds()) {
            builder.suggest(world.getName());
        }
        return builder.buildFuture();
    }

    private static void teleport(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var send = Optional.of(sender).filter(Player.class::isInstance).map(Player.class::cast);
        var targets = args.getOrSupply("targets", CommandArgs.supply(send::orElseThrow));
        var location = args.getOrThrow("location", Location.class);
        args.getOptional("rotation", Rotation.class).map(Rotation::getYaw).ifPresent(location::setYaw);
        args.getOptional("rotation", Rotation.class).map(Rotation::getPitch).ifPresent(location::setPitch);
        args.ifPresent("world", World.class, location::setWorld);
        targets.forEach(x -> x.teleport(location, PlayerTeleportEvent.TeleportCause.COMMAND));
        if (targets.isEmpty()) throw CommandAPI.failWithString("[NekoTools] プレイヤーが見つかりませんでした。");
    }
}
