package kame.nekotools.commands.execution;

import com.mojang.brigadier.LiteralMessage;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import dev.jorel.commandapi.CommandAPIBukkit;
import dev.jorel.commandapi.SuggestionInfo;
import dev.jorel.commandapi.arguments.*;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekocore.utils.StringUtils;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.NekoCommandAPI;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class ChainCommand {
    private static final String description =
            "§6Description:§r ChunkLoaderの機能を提供します。";
    private static final ArgumentSuggestions<CommandSender> commandSuggestions = new CommandArgument("").getOverriddenSuggestions().orElseThrow();

    static {
        // chain from @e for-all | /command arg0 arg1 arg2 | /command arg0 arg1 arg2 |
        // chain from @e if-true | /command arg0 arg1 arg2 | /command arg0 arg1 arg2 |
        // chain from sender [if-true|for-all] | /command...
        //
        Register.create("chain", description, "nekotools.command.chain")
                .withArguments(new MultiLiteralArgument("sender", "from", "proxy", "test"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new MultiLiteralArgument("type", "for-all", "if-each"))
                .withArguments(new GreedyStringArgument("commands").replaceSuggestions(ChainCommand::suggest))
                .executes(ChainCommand::forAll)
                .register();

        Register.create("chain", description, "nekotools.command.chain")
                .withArguments(new MultiLiteralArgument("type", "for-all", "if-each"))
                .withArguments(new GreedyStringArgument("commands").replaceSuggestions(ChainCommand::suggest))
                .executes(ChainCommand::forAll)
                .register();
    }

    private static CompletableFuture<Suggestions> suggest(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) throws CommandSyntaxException {
        if (info.currentArg().length() <= 1) {
            return suggestSplitter(info, builder);
        } else {
            var offset = builder.createOffset(builder.getStart() + info.currentArg().lastIndexOf(' ') + 1);
            if (offset.getRemaining().startsWith("@")) {
                return suggestDelay(info, offset);
            } else if (offset.getRemaining().startsWith("$")) {
                return suggestArgument(info, offset);
            } else {
                var override = builder.createOffset(builder.getStart() + info.currentArg().lastIndexOf(" | ") + 3);
                var suggestionInfo = new SuggestionInfo<>(info.sender(), info.previousArgs(), override.getInput(), override.getRemaining());
                return commandSuggestions.suggest(suggestionInfo, override);
            }
        }
    }

    private static CompletableFuture<Suggestions> suggestSplitter(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) {
        var suggest = Stream.of("|", "/", "-", "&", "%", "$", "#", "!", "+", ".");
        suggest.filter(x -> x.startsWith(info.currentArg())).forEach(builder::suggest);
        return builder.buildFuture();
    }

    private static CompletableFuture<Suggestions> suggestDelay(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) {
        builder.suggest("@delay:", new LiteralMessage("@delay:[ticks]"));
        return builder.buildFuture();
    }

    private static CompletableFuture<Suggestions> suggestArgument(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) {
        if (info.sender() instanceof Player player) {
            for (var method : StringUtils.getReplaceTags(player).entrySet()) {
                if (!method.getKey().toLowerCase().contains(builder.getRemainingLowerCase())) continue;
                try {
                    builder.suggest(method.getKey(), new LiteralMessage(Objects.toString(method.getValue().apply(player), "null")));
                } catch (Exception e) {
                    System.out.println(method.getKey());
                    e.printStackTrace();
                }
            }
        }
        return builder.buildFuture();
    }


    private static void forAll(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var commandLine = args.getOrThrow("commands", String.class);
        var splitter = " %s ".formatted(commandLine.charAt(0));
        var commands = Arrays.stream(StringUtils.split(commandLine.substring(1), splitter))
                .map(String::trim).filter(StringUtils::hasChars).toArray(String[]::new);
        var test = args.getOptional("test").isPresent();
        var type = args.getOptional("sender", String.class).orElse("");
        var chainIf = args.getOrThrow("type", String.class).equals("if-each");
        for (var target : args.getOrSupply("targets", CommandArgs.values(sender))) {
            var proxySender = switch (type) {
                case "proxy" -> NekoCommandAPI.wrapForProxySender(sender, sender, target);
                case "from" -> target;
                default -> sender;
            };
            dispatchCommand(proxySender, new ArrayDeque<>(List.of(commands)), chainIf, test);
        }
    }

    private static void dispatchCommand(CommandSender sender, Queue<String> commands, boolean chainIf, boolean test) throws WrapperCommandSyntaxException {
        if (commands.isEmpty()) return;
        var wrapper = NekoCommandAPI.wrapForBrigadierListener(sender);
        var replaced = StringUtils.replace(Objects.requireNonNull(commands.poll()), sender);
        if (test) sender.sendMessage(replaced);
        if (replaced.matches("^@delay:[0-9]+$")) {
            delay(sender, commands, chainIf, test, Long.parseLong(replaced.substring(7)));
        } else {
            try {
                CommandAPIBukkit.get().getBrigadierDispatcher().execute(replaced, wrapper);
            } catch (CommandSyntaxException e) {
                if (!CommandAPIBukkit.get().getSimpleCommandMap().dispatch(sender, replaced) && chainIf)
                    throw new WrapperCommandSyntaxException(e);
            }
            dispatchCommand(sender, commands, chainIf, test);
        }
    }

    private static void delay(CommandSender sender, Queue<String> commands, boolean chainIf, boolean test, long delay) {
        Bukkit.getScheduler().runTaskLater(NekoTools.getInstance(), () -> {
            try {
                dispatchCommand(sender, commands, chainIf, test);
            } catch (WrapperCommandSyntaxException e) {
                var component1 = new TextComponent(e.getMessage());
                component1.setColor(ChatColor.RED);
                var component2 = new TextComponent(e.getContext());
                component2.setColor(ChatColor.RED);
                sender.spigot().sendMessage(component1);
                sender.spigot().sendMessage(component2);
                throw new RuntimeException(e);
            }
        }, delay);
    }
}

