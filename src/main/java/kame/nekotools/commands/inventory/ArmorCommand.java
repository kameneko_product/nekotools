package kame.nekotools.commands.inventory;

import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import kame.nekocore.utils.ItemUtils;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Optional;

public class ArmorCommand {

    private static final String description =
            "§6Description:§r インベントリにある装備可能アイテムを装備させます。";

    static {
        // armor equip @a #minecraft:item{nbt}...
        // armor repair @a
        // armor equip @a inHand
        // armor set slot #minecraft:item{nbt}

        Register.create("armor", description, "nekotools.command.armor.equip.self")
                .withArguments(new LiteralArgument("equip"))
                .executesPlayer(ArmorCommand::equip)
                .register();

        Register.create("equip", description, "nekotools.command.armor.equip.self")
                .executesPlayer(ArmorCommand::wearing)
                .register();

        Register.create("armor", description, "nekotools.command.armor.equip.other")
                .withArguments(new LiteralArgument("equip"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .executes(ArmorCommand::equip)
                .register();

        Register.create("armor", description, "nekotools.command.armor.repair")
                .withArguments(new LiteralArgument("repair"))
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .executes(ArmorCommand::repair)
                .register();
    }

    private static void equip(CommandSender sender, CommandArgs args) {
        var send = Optional.of(sender).filter(Player.class::isInstance).map(Player.class::cast);
        var targets = args.getOrSupply("targets", CommandArgs.supply(send::orElseThrow));
        targets.forEach(ArmorCommand::equipArmors);
        if (targets.size() == 1) {
            var target = targets.iterator().next().getName();
            sender.sendMessage(String.format("§b[NekoTools] §r%sの防具を装着しました。", target));
        } else {
            var count = targets.size();
            sender.sendMessage(String.format("§b[NekoTools] §r%s人の防具を装着しました。", count));
        }
    }

    private static void equipArmors(Player player) {
        var inv = player.getInventory();
        for (var item : inv) {
            if (item == null) continue;
            var type = item.getType().toString();
            if (ItemUtils.isEmpty(inv.getHelmet()) && type.endsWith("_HELMET")) {
                inv.setHelmet(item.clone());
                item.setAmount(0);
            }
            if (ItemUtils.isEmpty(inv.getChestplate()) && type.endsWith("_CHESTPLATE")) {
                inv.setChestplate(item.clone());
                item.setAmount(0);
            }
            if (ItemUtils.isEmpty(inv.getLeggings()) && type.endsWith("_LEGGINGS")) {
                inv.setLeggings(item.clone());
                item.setAmount(0);
            }
            if (ItemUtils.isEmpty(inv.getBoots()) && type.endsWith("_BOOTS")) {
                inv.setBoots(item.clone());
                item.setAmount(0);
            }
        }
    }

    private static void wearing(Player sender, CommandArgs args) {
        var item = sender.getInventory().getItemInMainHand();
        if (ItemUtils.isEmpty(item)) {
            sender.sendMessage("§c[NekoTools] §r装備したいアイテムを手に持って実行してください。");
        } else if (ItemUtils.isEmpty(sender.getInventory().getHelmet())) {
            sender.getInventory().setHelmet(item);
            var name = Optional.ofNullable(item.getItemMeta()).map(ItemMeta::getDisplayName)
                    .or(() -> Optional.ofNullable(item.getItemMeta()).map(ItemMeta::getLocalizedName)).orElse(item.getType().toString());
            sender.sendMessage(String.format("§b[NekoTools] §r%s§rを頭に装備しました。", name));
            item.setAmount(0);
        } else {
            sender.sendMessage("§c[NekoTools] §r既に頭に装備している物があります。");
        }
    }

    private static void repair(CommandSender sender, CommandArgs args) {
        var optionTargets = args.getOrSupply("targets", CommandArgs::throwPlayers);
        optionTargets.forEach(ArmorCommand::repairArmors);
    }

    private static void repairArmors(Player player) {
        var inv = player.getInventory();
        repairItem(inv.getHelmet());
        repairItem(inv.getChestplate());
        repairItem(inv.getLeggings());
        repairItem(inv.getBoots());
    }

    private static void repairItem(ItemStack item) {
        if (ItemUtils.isNotEmpty(item) && item.getItemMeta() instanceof Damageable tools) {
            tools.setDamage(0);
        }
    }


}
