package kame.nekotools.commands.inventory;

import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import dev.jorel.commandapi.SuggestionInfo;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.ItemStackPredicateArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class DropCommand {
    private static final String description =
            "§6Description:§r プレイヤーのインベントリのアイテムをドロップさせます。";

    static {
        // drop @p #minecraft:item{nbt} all pickup_delay
        // drop @p #minecraft:item{nbt} amount pickup_delay
        // drop @p any all pickup_delay
        // drop @p any amount pickup_delay
        Register.create("drop", description, "nekotools.command.drop")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withOptionalArguments(new LiteralArgument("#any"), new LiteralArgument("#all"))
                .withOptionalArguments(new IntegerArgument("pickup_delay"))
                .executes(DropCommand::drop)
                .register();
        Register.create("drop", description, "nekotools.command.drop")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LiteralArgument("#any"))
                .withArguments(new IntegerArgument("count", 0).replaceSuggestions(DropCommand::suggest))
                .withOptionalArguments(new IntegerArgument("pickup_delay"))
                .executes(DropCommand::drop)
                .register();
        Register.create("drop", description, "nekotools.command.drop")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withOptionalArguments(new ItemStackPredicateArgument("types"))
                .withOptionalArguments(new LiteralArgument("#all"))
                .withOptionalArguments(new IntegerArgument("pickup_delay"))
                .executes(DropCommand::drop)
                .register();
        Register.create("drop", description, "nekotools.command.drop")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new ItemStackPredicateArgument("types"))
                .withArguments(new IntegerArgument("count", 0).replaceSuggestions(DropCommand::suggest))
                .withOptionalArguments(new IntegerArgument("pickup_delay"))
                .executes(DropCommand::drop)
                .register();
    }

    private static CompletableFuture<Suggestions> suggest(SuggestionInfo<CommandSender> sender, SuggestionsBuilder builder) {
        return builder.suggest(1).suggest(16).suggest(32).suggest(64).buildFuture();
    }

    private static int drop(CommandSender sender, CommandArgs args) {
        var targets = args.getOrSupply("targets", CommandArgs::throwPlayers);
        var typeFilter = args.getOrSupply("types", CommandArgs.itemPredicate(x -> true));
        var pickupDelay = args.getOptional("pickup_delay").map(Integer.class::cast);
        int count = args.getOrSupply("count", CommandArgs.value(-1));
        var counts = 0;
        for (var target : targets) {
            for (var remove : removeItems(target.getInventory(), typeFilter, count)) {
                counts += remove.getAmount();
                var dropItem = target.getWorld().dropItem(target.getLocation().add(0, 0.8, 0), remove);
                pickupDelay.ifPresent(dropItem::setPickupDelay);
                dropItem.setThrower(target.getUniqueId());
            }
        }
        return counts;
    }

    private static ItemStack[] removeItems(Inventory inventory, Predicate<ItemStack> filter, int amount) {
        var removeItems = new ArrayList<ItemStack>();
        var items = inventory.getContents();
        for (var item : Stream.of(items).filter(Objects::nonNull).filter(filter).toList()) {
            var remove = item.clone();
            if (amount >= 0) {
                if (remove.getAmount() > amount) remove.setAmount(amount);
                amount -= remove.getAmount();
            }
            item.setAmount(item.getAmount() - remove.getAmount());
            if (remove.getAmount() > 0) removeItems.add(remove);
        }
        return removeItems.toArray(ItemStack[]::new);
    }

}
