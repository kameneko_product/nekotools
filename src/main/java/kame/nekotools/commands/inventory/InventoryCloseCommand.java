package kame.nekotools.commands.inventory;

import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.ListArgumentBuilder;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.event.inventory.InventoryType;

public class InventoryCloseCommand {

    private static final String description =
            "§6Description:§r プレイヤーがチェスト等を開いていた場合強制的に閉じさせます。";

    static {
        Register.create("invclose", description, "nekotools.command.invclose")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .executes(InventoryCloseCommand::invClose)
                .register();

        Register.create("invclose", description, "nekotools.command.invclose")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new LiteralArgument("force"))
                .executes(InventoryCloseCommand::forceInvClose)
                .register();

        Register.create("invclose", description, "nekotools.command.invclose")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new ListArgumentBuilder<InventoryType>("types")
                        .allowDuplicates(false)
                        .withList(InventoryType.values())
                        .withMapper(x -> StringUtils.capitalize(x.toString().replace('_', ' ')).replace(" ", ""))
                        .buildText())
                .executes(InventoryCloseCommand::invClose)
                .register();
    }

    private static int invClose(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var players = args.getOrSupply("targets", CommandArgs::throwPlayers);
        var type = args.getOrSupply("types", CommandArgs.empty(InventoryType.class));
        var successCount = 0;
        for (var player : players) {
            var inv = player.getOpenInventory();
            if (type.isEmpty()) {
                if (inv.getType() != InventoryType.CRAFTING && inv.getType() != InventoryType.CREATIVE) {
                    inv.close();
                    successCount++;
                }
            } else if (type.contains(inv.getType())) {
                inv.close();
                successCount++;
            }
        }
        if (successCount > 0) {
            sender.sendMessage(successCount + "人のプレイヤーのインベントを閉じさせました。");
        } else {
            throw CommandAPI.failWithString("該当するプレイヤーが見つかりませんでした。");
        }
        return successCount;
    }

    private static int forceInvClose(CommandSender sender, CommandArgs args) throws WrapperCommandSyntaxException {
        var successCount = 0;
        for (var player : args.getOrSupply("targets", CommandArgs::throwPlayers)) {
            player.getOpenInventory().close();
            successCount++;
        }
        if (successCount > 0) {
            sender.sendMessage(successCount + "人のプレイヤーのインベントを閉じさせました。");
        } else {
            throw CommandAPI.failWithString("該当するプレイヤーが見つかりませんでした。");
        }
        return successCount;
    }

}
