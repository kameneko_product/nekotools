package kame.nekotools.commands.inventory;

import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.arguments.MultiLiteralArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import kame.nekotools.inventory.invswitch.InventoryStoreType;
import kame.nekotools.inventory.invswitch.InventorySwitchManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.concurrent.CompletableFuture;

public class InventorySwitchCommand {

    private static final String description =
            "§6Description:§r インベントリの切り替え機能を提供します。";

    static {
        // invswitch @p inventory

        Register.create("invswitch", description, "nekotools.command.invswitch")
                .withOptionalArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .executesPlayer(InventorySwitchCommand::showSelectedInventory)
                .register();

        Register.create("invswitch", description, "nekotools.command.invswitch")
                .withArguments(new EntitySelectorArgument.ManyPlayers("targets"))
                .withArguments(new StringArgument("inventory_name")
                        .replaceSuggestions((x, y) -> listInventories(y)))
                .executes(InventorySwitchCommand::switchInventory)
                .register();

        Register.create("invswitch-manage", description, "nekotools.command.invswitch")
                .withArguments(new LiteralArgument("list"))
                .executesPlayer(InventorySwitchCommand::listInventory)
                .register();

        Register.create("invswitch-manage", description, "nekotools.command.invswitch")
                .withArguments(new LiteralArgument("type"))
                .withArguments(new StringArgument("inventory_name")
                        .replaceSuggestions((x, y) -> listInventories(y)))
                .withArguments(new MultiLiteralArgument("inventory_type", "Default", "Static"))
                .executesPlayer(InventorySwitchCommand::setInventoryType)
                .register();

        Register.create("invswitch-manage", description, "nekotools.command.invswitch")
                .withArguments(new LiteralArgument("data"))
                .withArguments(new StringArgument("inventory_name")
                        .replaceSuggestions((x, y) -> listInventories(y)))
                .withOptionalArguments(new StringArgument("force"))
                .executesPlayer(InventorySwitchCommand::setInventoryData)
                .register();
    }

    private static void showSelectedInventory(Player sender, CommandArgs args) {
        for (var target : args.getOrSupply("targets", CommandArgs.values(sender))) {
            var invName = InventorySwitchManager.getInstance().getSelectedInventory(target);
            var message = String.format("%sのインベントリは現在%sが設定されています。", target.getName(), invName);
            sender.sendMessage(String.format("§b[NekoTools] §r%s", message));
        }
    }

    private static void switchInventory(CommandSender sender, CommandArgs args) {
        var targets = args.getOrSupply("targets", CommandArgs::throwPlayers);
        var invName = args.getOrSupply("inventory_name", CommandArgs.thrown(String.class));
        for (var target : targets) {
            InventorySwitchManager.getInstance().switchInventory(target, invName);
        }
        if (targets.size() == 1) {
            var target = targets.iterator().next().getName();
            var message = String.format("%sの持ち物、体力、満腹度、レベル、エフェクト値を%sに設定しました", target, invName);
            sender.sendMessage(String.format("§b[NekoTools] §r%s", message));
        } else {
            var count = targets.size();
            var message = String.format("%s人の持ち物、体力、満腹度、レベル、エフェクト値を%sに設定しました", count, invName);
            sender.sendMessage(String.format("§b[NekoTools] §r%s", message));
        }
    }

    private static void listInventory(CommandSender sender, CommandArgs args) {
        sender.sendMessage("§b[NekoTools] §rインベントリ一覧を表示します");
        sender.sendMessage("§aDefault§6:§r 通常インベントリ、プレイヤー毎に内容が保存されます。");
        sender.sendMessage("§bStatic§6:§r 固定インベントリ、設定した内容が適用され保存されません。");
        var message = new TextComponent("[");
        var first = true;
        for (var entry : InventorySwitchManager.getInstance().getKnownInventories().entrySet()) {
            if (first) first = false;
            else message.addExtra(", ");
            message.addExtra(new TextComponent(entry.getKey()));
            message.addExtra(new TextComponent("§6:§r"));
            var type = new TextComponent(entry.getValue().toString());
            type.setColor(entry.getValue() == InventoryStoreType.Static ? ChatColor.AQUA : ChatColor.GREEN);
            message.addExtra(type);
        }
        message.addExtra("]");
        sender.spigot().sendMessage(message);
    }

    private static void setInventoryType(CommandSender sender, CommandArgs args) {
        var invName = args.getOrSupply("inventory_name", CommandArgs.thrown(String.class));
        var invType = args.getOrSupply("inventory_type", CommandArgs.thrown(String.class));
        var type = InventoryStoreType.valueOf(invType);
        if (InventorySwitchManager.getInstance().setKnownInventoryType(invName, type)) {
            var typeStr = type == InventoryStoreType.Default ? "§aDefault§r" : "§bStatic§r";
            sender.sendMessage(String.format("§b[NekoTools] §r[%s]のインベントリタイプを%sに設定しました。", invName, typeStr));
        } else {
            var typeStr = type == InventoryStoreType.Default ? "§aDefault§r" : "§bStatic§r";
            sender.sendMessage(String.format("§b[NekoTools] §r[%s]のインベントリタイプは既にを%sです。", invName, typeStr));
        }
    }

    private static void setInventoryData(Player sender, CommandArgs args) {
        var invName = args.getOrSupply("inventory_name", CommandArgs.thrown(String.class));
        if (InventorySwitchManager.getInstance().getKnownInventories().get(invName) == InventoryStoreType.Default) {
            sender.sendMessage("§c[NekoTools] TypeがStaticに設定されている物だけ設定が出来ます。");
            return;
        }
        var force = args.getOptional("force").filter("--force"::equals).isPresent();
        if (!InventorySwitchManager.getInstance().hasTemplate(invName) || force) {
            InventorySwitchManager.getInstance().setTemplateData(invName, sender);
            sender.sendMessage(String.format("§b[NekoTools] §r[%s]に現在の持ち物、体力、満腹度、レベル、エフェクト値を設定しました", invName));
        } else {
            sender.sendMessage(String.format("[%s]には既に設定がされています、上書きしますか？", invName));
            var message = new TextComponent(String.format("[%s]には既に設定がされています、上書きしますか？ ", invName));
            var yes = new TextComponent("[はい]");
            yes.setUnderlined(true);
            var command = String.format("/invswitch-manage data %s --force", invName);
            yes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
            yes.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("クリックで実行します。")));
            message.addExtra(yes);
            sender.spigot().sendMessage(new TextComponent("§b[NekoTools] §r"), message);
        }
    }

    private static CompletableFuture<Suggestions> listInventories(SuggestionsBuilder builder) {
        for (var name : InventorySwitchManager.getInstance().getKnownInventories().keySet()) {
            builder.suggest(name);
        }
        return builder.buildFuture();
    }

}
