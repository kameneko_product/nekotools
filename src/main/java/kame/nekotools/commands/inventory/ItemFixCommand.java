package kame.nekotools.commands.inventory;

import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import kame.nekotools.inventory.itemfix.gui.ItemFix;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class ItemFixCommand {

    private static final String description =
            "§6Description:§r 手に持ってるアイテムを編集します。";

    static {
        Register.create("itemfix", description, "nekotools.command.itemfix")
                .executesPlayer(ItemFixCommand::itemfix)
                .register();
    }

    public static boolean itemfix(Player player, CommandArgs args) {
        boolean isOpened = ItemFix.openGui(player, player.getInventory().getItemInMainHand());
        if (isOpened) {
            player.playSound(player, Sound.BLOCK_SHULKER_BOX_OPEN, 10, 2);
        } else {
            player.sendMessage("[itemfix] §c引数無しのitemfixは手にアイテムを持つ必要があります。");
            player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
        }
        return true;
    }
}
