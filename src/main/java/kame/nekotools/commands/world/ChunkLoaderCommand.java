package kame.nekotools.commands.world;

import dev.jorel.commandapi.arguments.EntitySelectorArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import kame.nekotools.world.chunkloader.ChunkLoadManager;
import kame.nekotools.world.chunkloader.ChunkLoader;
import kame.nekotools.world.chunkloader.ChunkLoaderListener;
import kame.nekotools.world.chunkloader.ChunkLoaderViewer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ChunkLoaderCommand {

    private static BukkitTask task;

    private static final Set<Player> displayTargets = new HashSet<>();

    private static final String description =
            "§6Description:§r ChunkLoaderの機能を提供します。";

    static {
        // chunkloader @p
        // chunkloader list
        // chunkloader list on
        // chunkloader list off
        Register.create("chunkloader", description, "nekotools.command.chunkloader")
                .withOptionalArguments(new EntitySelectorArgument.ManyPlayers("target"))
                .executesPlayer(ChunkLoaderCommand::takeOut)
                .register();

        Register.create("chunkloader", description, "nekotools.command.chunkloader")
                .withArguments(new LiteralArgument("list"))
                .executesPlayer(ChunkLoaderCommand::toggleDisplay)
                .register();

        Register.create("chunkloader", description, "nekotools.command.chunkloader")
                .withArguments(new LiteralArgument("list"))
                .withArguments(new LiteralArgument("on"))
                .executesPlayer(ChunkLoaderCommand::startDisplay)
                .register();

        Register.create("chunkloader", description, "nekotools.command.chunkloader")
                .withArguments(new LiteralArgument("list"))
                .withArguments(new LiteralArgument("off"))
                .executesPlayer(ChunkLoaderCommand::stopDisplay)
                .register();
        NekoTools.getInstance().addRunOnEnablingQueue(ChunkLoaderCommand::initialize);
    }

    private static void initialize() {
        Bukkit.getPluginManager().registerEvents(new ChunkLoaderListener(), NekoTools.getInstance());
    }

    private static void takeOut(Player sender, CommandArgs args) {
        var targets = args.getOrSupply("target", CommandArgs.values(sender));
        targets.stream().map(Player::getInventory).forEach(x -> x.addItem(ChunkLoadManager.getItem()));
    }

    private static void toggleDisplay(Player sender, CommandArgs args) {
        sendChunkListMessage(sender);
        if (!displayTargets.contains(sender)) {
            startDisplay(sender, args);
            sender.sendMessage("§b[NekoTools] §r表示をOFFにする場合はもう一度コマンドを入力します。");
        } else {
            stopDisplay(sender, args);
        }
    }

    private static void startDisplay(Player sender, CommandArgs args) {
        if (task == null) {
            task = Bukkit.getScheduler().runTaskTimer(NekoTools.getInstance(), ChunkLoaderCommand::chunkDisplayTask, 0, 20);
        }
        sender.sendMessage("§b[NekoTools] §rチャンクローダが設置されている一覧をパーティクル表示します。");
        displayTargets.add(sender);
    }

    private static void stopDisplay(Player sender, CommandArgs args) {
        sender.sendMessage("§b[NekoTools] §rチャンクローダが設置されている一覧の表示を解除しました。");
        displayTargets.remove(sender);
    }

    private static void sendChunkListMessage(Player sender) {
        var groups = ChunkLoadManager.getLoadedChunks().stream().collect(Collectors.groupingBy(Chunk::getWorld));
        for (var entry : groups.entrySet()) {
            var world = entry.getKey();
            var component = new TextComponent(" ");
            for (var chunk : entry.getValue()) {
                var loader = ChunkLoadManager.findFromChunk(chunk);
                var chunkText = new TextComponent(String.format("%s %s", chunk.getX(), chunk.getZ()));
                chunkText.setUnderlined(loader.isPresent());
                if (loader.isPresent() && loader.map(ChunkLoader::getWorld).filter(sender.getWorld()::equals).isPresent()) {
                    var block = loader.get().getLocation().getBlock();
                    var command = String.format("/tp %s %s %s", block.getX(), block.getY(), block.getZ());
                    chunkText.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command));
                    chunkText.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("チャンクローダへテレポート")));
                }
                component.addExtra(new TextComponent(new TextComponent("§6["), chunkText, new TextComponent("§6]")));
                component.addExtra(new TextComponent("§b,§r"));
            }
            sender.sendMessage(String.format("§a%s §6(§a%s§6)§r:", world.getName(), world.getUID()));
            sender.spigot().sendMessage(component);
        }
    }

    public static void chunkDisplayTask() {
        if (displayTargets.isEmpty()) return;
        displayTargets.removeIf(x -> !x.isOnline());
        for (var chunk : ChunkLoadManager.getLoadedChunks()) {
            if (displayTargets.stream().noneMatch(x -> x.getWorld() == chunk.getWorld())) continue;
            var chunkPos = new Vector(chunk.getX(), 0, chunk.getZ());
            var targets = displayTargets.stream().filter(player -> {
                var playerChunk = player.getLocation().getChunk();
                var playerPos = new Vector(playerChunk.getX(), 0, playerChunk.getZ());
                return chunkPos.distance(playerPos) < player.getClientViewDistance();
            }).toList();
            if (targets.isEmpty()) continue;
            targets.forEach(x -> ChunkLoaderViewer.showEffect(x, chunk, +0));
            targets.forEach(x -> ChunkLoaderViewer.showEffect(x, chunk, -5));
            targets.forEach(x -> ChunkLoaderViewer.showEffect(x, chunk, +5));
        }
    }
}
