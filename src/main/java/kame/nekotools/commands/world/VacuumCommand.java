package kame.nekotools.commands.world;

import com.mojang.brigadier.suggestion.Suggestions;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;
import dev.jorel.commandapi.CommandAPI;
import dev.jorel.commandapi.SuggestionInfo;
import dev.jorel.commandapi.arguments.ArgumentSuggestions;
import dev.jorel.commandapi.arguments.DoubleArgument;
import dev.jorel.commandapi.arguments.ItemStackArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import kame.nekocore.utils.StringUtils;
import kame.nekotools.NekoTools;
import kame.nekotools.commands.wrapper.Register;
import kame.nekotools.commands.wrapper.executors.CommandArgs;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;
import java.util.concurrent.CompletableFuture;

public class VacuumCommand {

    private static final String description =
            "§6Description:§r プレイヤーの周囲のアイテムを引き寄せます。";

    static {
        Register.create("vacuum", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("on"))
                .executesPlayer(VacuumCommand::on)
                .register();
        Register.create("vacuum", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("on"))
                .withArguments(new DoubleArgument("range", 0)
                        .replaceSuggestions(ArgumentSuggestions.strings("5", "10", "20", "100")))
                .executesPlayer(VacuumCommand::on)
                .register();
        Register.create("vacuum", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("off"))
                .executesPlayer(VacuumCommand::off)
                .register();
        Register.create("vacuum", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("add"))
                .withArguments(new ItemStackArgument("material"))
                .executesPlayer(VacuumCommand::add)
                .register();
        Register.create("vacuum", description, "nekotools.command.vacuum")
                .withArguments(new LiteralArgument("remove"))
                .withArguments(new ItemStackArgument("material")
                        .replaceSuggestions(VacuumCommand::suggest))
                .executesPlayer(VacuumCommand::remove)
                .register();

    }

    private static CompletableFuture<Suggestions> suggest(SuggestionInfo<CommandSender> info, SuggestionsBuilder builder) {
        if (info.sender() instanceof Player player) {
            var vacuum = map.getOrDefault(player.getUniqueId(), new Vacuum(0, new HashSet<>()));
            vacuum.materials.stream().map(Material::getKey).map(NamespacedKey::toString).forEach(builder::suggest);
        }
        return builder.buildFuture();
    }

    private static void on(Player player, CommandArgs args) {
        var range = args.getOrSupply("range", CommandArgs.value(5.0));
        var old = map.put(player.getUniqueId(), new Vacuum(range, new HashSet<>()));
        if (old != null) map.get(player.getUniqueId()).materials.addAll(old.materials);
        StringUtils.sendMessage(player, "[vacuum] §eアイテム吸い寄せ §a有効");
        if (task == null) {
            task = Bukkit.getScheduler()
                    .runTaskTimer(NekoTools.getPlugin(NekoTools.class), VacuumCommand::run, 1, 1);
        }
    }

    private static void off(Player player, CommandArgs args) {
        map.remove(player.getUniqueId());
        StringUtils.sendMessage(player, "[vacuum] §eアイテム吸い寄せ §c無効");
    }

    private static void add(Player player, CommandArgs args) throws WrapperCommandSyntaxException {
        if (!map.containsKey(player.getUniqueId())) {
            throw CommandAPI.failWithString("[vacuum] §cアイテム吸い寄せが有効ではありません。");
        }
        var item = args.getOrSupply("material", CommandArgs.thrown(ItemStack.class));
        map.get(player.getUniqueId()).materials.add(item.getType());
        StringUtils.sendMessage(player, "[vacuum] §a" + item.getType() + "§eを引き寄せ一覧に追加しました");
    }

    private static void remove(Player player, CommandArgs args) throws WrapperCommandSyntaxException {
        if (!map.containsKey(player.getUniqueId())) {
            throw CommandAPI.failWithString("[vacuum] §cアイテム吸い寄せが有効ではありません。");
        }
        var item = args.getOrSupply("material", CommandArgs.thrown(ItemStack.class));
        map.get(player.getUniqueId()).materials.remove(item.getType());
        StringUtils.sendMessage(player, "[vacuum] §a" + item.getType() + "§eを引き寄せ一覧から除外しました");
    }

    private static final Map<UUID, Vacuum> map = new HashMap<>();

    private static BukkitTask task;

    private static void run() {
        map.forEach(VacuumCommand::run);
    }

    private static void run(UUID uuid, Vacuum data) {
        var player = Bukkit.getPlayer(uuid);
        if (player == null || !player.isOnline()) return;
        var r = data.range;
        var loc = player.getEyeLocation();
        for (var entity : player.getWorld().getNearbyEntities(loc, r, r, r, Item.class::isInstance)) {
            if (entity instanceof Item item && !data.materials.isEmpty() && !data.materials.contains(item.getItemStack().getType())) {
                continue;
            }
            var entityLoc = entity.getLocation();
            var range = entityLoc.distance(loc);
            if (range < data.range) {
                var sub = entityLoc.subtract(loc).toVector();
                var vec = entity.getVelocity().subtract(sub.multiply(1 / range / 10));
                entity.setVelocity(vec);
            }
        }
    }

    private record Vacuum(double range, Set<Material> materials) {
        public static final Vacuum Empty = new Vacuum(0, Set.of());
    }
}
