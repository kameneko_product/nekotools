package kame.nekotools.commands.wrapper;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.Argument;
import kame.nekotools.commands.wrapper.executors.CommandExecution;
import kame.nekotools.commands.wrapper.executors.ResultingCommandExecution;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class Register {

    public static CommandBuilder create(String name, String shortDescription, String permission) {
        return new CommandBuilder(new CommandAPICommand(name)
                .withShortDescription(shortDescription)
                .withPermission(permission));
    }

    public static class CommandBuilder {
        private final CommandAPICommand command;

        private CommandBuilder(CommandAPICommand apiCommand) {
            command = apiCommand;
        }

        public CommandBuilder withArguments(Argument<?>... arguments) {
            command.withArguments(arguments);
            return this;
        }

        public CommandBuilder withOptionalArguments(Argument<?>... arguments) {
            command.withOptionalArguments(arguments);
            return this;
        }

        public CommandBuilder executes(CommandExecution<CommandSender> executes) {
            command.executes(executes::asApi);
            return this;
        }

        public CommandBuilder executesPlayer(CommandExecution<Player> executes) {
            command.executesPlayer(executes::asApi);
            return this;
        }

        public CommandBuilder executesPlayer(ResultingCommandExecution<Player> executes) {
            command.executesPlayer(executes::asApi);
            return this;
        }

        public CommandBuilder executes(ResultingCommandExecution<CommandSender> executes) {
            command.executes(executes::asApi);
            return this;
        }

        public CommandBuilder executesCommandBlock(CommandExecution<BlockCommandSender> executes) {
            command.executesCommandBlock(executes::asApi);
            return this;
        }

        public CommandBuilder executesCommandBlock(ResultingCommandExecution<BlockCommandSender> executes) {
            command.executesCommandBlock(executes::asApi);
            return this;
        }

        public void register() {
            command.register();
        }

    }

}
