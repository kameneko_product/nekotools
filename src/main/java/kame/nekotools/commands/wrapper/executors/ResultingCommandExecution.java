package kame.nekotools.commands.wrapper.executors;

import dev.jorel.commandapi.exceptions.WrapperCommandSyntaxException;
import dev.jorel.commandapi.executors.CommandArguments;
import org.bukkit.command.CommandSender;

public interface ResultingCommandExecution<Sender extends CommandSender> {
    int execute(Sender sender, CommandArgs args) throws WrapperCommandSyntaxException;

    default int asApi(Sender sender, CommandArguments args) throws WrapperCommandSyntaxException {
        return execute(sender, new CommandArgs(args));
    }
}
