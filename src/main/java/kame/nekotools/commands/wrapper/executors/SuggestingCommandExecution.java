package kame.nekotools.commands.wrapper.executors;

import dev.jorel.commandapi.SuggestionInfo;
import org.bukkit.command.CommandSender;

public interface SuggestingCommandExecution<Sender extends CommandSender> {
    String[] execute(Sender sender, CommandArgs args);

    default String[] asApi(SuggestionInfo<Sender> info) {
        return execute(info.sender(), new CommandArgs(info.previousArgs()));
    }
}