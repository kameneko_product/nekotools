package kame.nekotools.inventory.invswitch.repositories;

import kame.nekotools.NekoTools;
import kame.nekotools.inventory.invswitch.InventoryStoreType;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class KnownInventories {
    private final Map<String, InventoryStoreType> knownInventories = new HashMap<>();
    private final File invPath = new File(NekoTools.getInstance().getDataFolder(), "knownInventories");

    public KnownInventories() {
        var known = invPath.exists() ? YamlConfiguration.loadConfiguration(invPath) : new YamlConfiguration();
        for (var key : known.getKeys(false)) {
            try {
                knownInventories.put(key, InventoryStoreType.valueOf(known.getString(key)));
            } catch (Exception ignored) {

            }
        }
        setKnownInventoryType("default", InventoryStoreType.Default);
    }


    public boolean isDefaultType(String inventoryName) {
        checkKnownInventory(inventoryName);
        return knownInventories.get(inventoryName) == InventoryStoreType.Default;
    }

    public boolean isStaticType(String inventoryName) {
        checkKnownInventory(inventoryName);
        return knownInventories.get(inventoryName) == InventoryStoreType.Static;
    }

    public Map<? extends String, ? extends InventoryStoreType> getKnownInventories() {
        return Collections.unmodifiableMap(knownInventories);
    }

    public boolean setKnownInventoryType(String name, InventoryStoreType type) {
        if (knownInventories.put(name, type) != type) {
            Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), this::save);
            return true;
        }
        return false;
    }

    private void checkKnownInventory(String inventoryName) {
        if (!knownInventories.containsKey(inventoryName)) {
            knownInventories.put(inventoryName, InventoryStoreType.Default);
            Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), this::save);
        }
    }

    private void save() {
        try {
            var known = new YamlConfiguration();
            for (var entry : knownInventories.entrySet()) {
                known.set(entry.getKey(), entry.getValue().toString());
            }
            known.save(invPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
