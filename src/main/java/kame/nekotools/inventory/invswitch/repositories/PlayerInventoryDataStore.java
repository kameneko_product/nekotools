package kame.nekotools.inventory.invswitch.repositories;

import kame.nekotools.NekoTools;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerInventoryDataStore {
    public interface EndSaveCallback {
        void endSave();
    }

    private static final ConcurrentHashMap<UUID, PlayerDataStore> playerDataStore = new ConcurrentHashMap<>();

    public boolean unloadPlayerData(@NotNull UUID player) {
        return playerDataStore.remove(player) != null;
    }

    public void save() {
        playerDataStore.values().forEach(PlayerDataStore::save);
    }

    public void save(@NotNull UUID player) {
        var data = playerDataStore.remove(player);
        if (data != null) data.save();
    }

    public void beginSave(@NotNull UUID player, @NotNull EndSaveCallback callback) {
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), () -> {
            save(player);
            callback.endSave();
        });
    }

    public void beginSave(EndSaveCallback callback) {
        Bukkit.getScheduler().runTaskAsynchronously(NekoTools.getInstance(), () -> {
            save();
            callback.endSave();
        });
    }
}