package kame.nekotools.inventory.invswitch.repositories.models;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.jetbrains.annotations.NotNull;

public record StatusData(double health, int food, float saturation, int level, float exp,
                         @NotNull PotionEffect[] effects) {
    public StatusData() {
        this(20, 20, 20, 0, 0, new PotionEffect[0]);
    }

    public StatusData(@NotNull Player player) {
        this(player.getHealth(), player.getFoodLevel(), player.getSaturation(), player.getLevel(), player.getExp(),
                player.getActivePotionEffects().toArray(PotionEffect[]::new));
    }
}
