package kame.nekotools.inventory.invswitch.repositories.yaml;

import kame.nekotools.NekoTools;
import kame.nekotools.inventory.invswitch.repositories.PlayerDataStore;
import kame.nekotools.inventory.invswitch.repositories.models.PlayerData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class YamlPlayerDataStore implements PlayerDataStore {
    private static final File folderPath = new File(NekoTools.getInstance().getDataFolder(), "inventories");
    private static final boolean created = folderPath.mkdir();

    private final File file;
    private final YamlConfiguration root;
    private final ConfigurationSection tables;

    static {
        ConfigurationSerialization.registerClass(YamlPlayerData.class, "PlayerData");
    }

    public YamlPlayerDataStore(@NotNull UUID playerId) {
        this(playerId.toString());
    }

    public YamlPlayerDataStore(@NotNull String name) {
        file = new File(folderPath, String.format("%s", name));
        var temp = new YamlConfiguration();
        if (file.exists()) {
            try (var reader = new InputStreamReader(new GZIPInputStream(new FileInputStream(file)))) {
                temp = YamlConfiguration.loadConfiguration(reader);
            } catch (Exception e) {
                temp = YamlConfiguration.loadConfiguration(file);
            }
        }
        root = temp;
        if (root.isConfigurationSection("tables")) {
            tables = root.getConfigurationSection("tables");
        } else {
            tables = root.createSection("tables");
        }
    }

    @Override
    public boolean hasData(@NotNull String name) {
        return tables.contains(name);
    }

    @Override
    public PlayerData getData(@NotNull String name) {
        return tables.get(name) instanceof YamlPlayerData d ? d : new YamlPlayerData();
    }

    @Override
    public void setData(@NotNull Player player, @NotNull String name) {
        tables.set(name, new YamlPlayerData(player));
    }

    @Override
    public String getSelectedName() {
        return root.getString("selected", "default");
    }

    @Override
    public void setSelectedName(@NotNull String name) {
        root.set("selected", name);
    }

    @Override
    public void save() {
        try (var writer = new OutputStreamWriter(new GZIPOutputStream(new FileOutputStream(file)))) {
            writer.write(root.saveToString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}