package kame.nekotools.inventory.itemfix.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

public class ItemFixEnterAddLoreEvent extends ItemFixBaseEvent {

    private static final HandlerList handlers = new HandlerList();
    private final int index;

    public ItemFixEnterAddLoreEvent(Player player, ItemStack item, int index) {
        super(player, item);
        this.index = index;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public int getIndex() {
        return index;
    }
}
