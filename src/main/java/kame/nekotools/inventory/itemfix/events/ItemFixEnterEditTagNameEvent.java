package kame.nekotools.inventory.itemfix.events;

import kame.tagapi.TagItem;
import kame.tagapi.TagSection;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Stack;

public class ItemFixEnterEditTagNameEvent extends ItemFixBaseEvent {

    private static final HandlerList handlers = new HandlerList();
    private final TagItem tagItem;
    private final TagSection data;
    private final Stack<String> paths;
    private final String name;
    private final int page;

    public ItemFixEnterEditTagNameEvent(Player player, TagItem item, TagSection data, Stack<String> paths, String name, int page) {
        super(player, item.getItemStack());
        this.data = data;
        this.tagItem = item;
        this.paths = paths;
        this.name = name;
        this.page = page;
    }

    @NotNull
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public TagItem getTagItem() {
        return tagItem;
    }

    public TagSection getData() {
        return data;
    }

    public Stack<String> getPaths() {
        return paths;
    }

    public String getName() {
        return name;
    }

    public int getPage() {
        return page;
    }
}
