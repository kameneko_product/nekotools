package kame.nekotools.inventory.itemfix.gui;

import kame.nekocore.inventory.ActionInventory;
import kame.nekocore.inventory.ActionItemStack;
import kame.nekotools.inventory.itemfix.events.ItemFixEnterSetLocNameEvent;
import kame.nekotools.inventory.itemfix.events.ItemFixEnterSetNameEvent;
import kame.tagapi.TagItem;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;
import java.util.Optional;
import java.util.Stack;
import java.util.function.Consumer;

public class ItemFix {

    public static boolean openGui(Player player, ItemStack item) {
        var meta = item.getItemMeta();
        if (meta != null) {
            var inv = new ActionInventory("ItemFix GUI editor", 18);
            inv.setItem(4, item);
            inv.setItem(9, new ActionItemStack(Material.NAME_TAG, "§bアイテム名編集") {{
                addClickEvent(x -> onEditNameClicked(player, item));
            }});
            inv.setItem(10, new ActionItemStack(Material.FILLED_MAP, "§bローカライズ名編集") {{
                addClickEvent(x -> onEditLocNameClicked(player, item));
            }});
            inv.setItem(11, new ActionItemStack(Material.JUNGLE_SIGN, "§bLore編集") {{
                addClickEvent(x -> onEditLoreClicked(player, item));
            }});
            inv.setItem(12, new ActionItemStack(Material.REDSTONE_TORCH, "§bHideFlag編集") {{
                addClickEvent(x -> onEditHideFlagClicked(player, item));
            }});
            inv.setItem(14, new ActionItemStack(Material.ENCHANTED_BOOK, "§bエンチャント編集") {{
                addClickEvent(x -> onEditEnchantmentClicked(player, item));
                addUnsafeEnchantments(item.getEnchantments());
                removeHideFlags(ItemFlag.HIDE_ENCHANTS);
            }});
            inv.setItem(15, new ActionItemStack(meta.isUnbreakable() ? Material.MUSIC_DISC_STAL : Material.MUSIC_DISC_11) {{
                setDisplayName("§bUnbreakable設定§f(%s§f)".formatted(meta.isUnbreakable() ? "§a有効" : "§c無効"));
                addClickEvent(x -> onEditUnbreakableClicked(player, item));
            }});
            inv.setItem(16, new ActionItemStack(Material.ARMOR_STAND) {{
                var model = meta.hasCustomModelData() ? meta.getCustomModelData() : null;
                setDisplayName("§bCustomModelData値§f(§e%d§f)".formatted(model));
                setLore("§f左クリック§6:§a増加", "§f右クリック§6:§c減少", "§fドロップ§6:§b初期値");
                addClickEvent(x -> onEditModelDataClicked(x, player, item));
            }});
            inv.setItem(17, new ActionItemStack(Material.NETHER_STAR, "§bNBT編集") {{
                addClickEvent(x -> onEditTagClicked(player, item));
            }});
            inv.openInventory(player);
            return true;
        } else {
            return false;
        }
    }

    private static void onEditNameClicked(Player player, ItemStack item) {
        var event = new ItemFixEnterSetNameEvent(player, item);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            player.playSound(player, Sound.ENTITY_CAT_AMBIENT, 10, 2);
            player.sendMessage("[itemfix] §dチャットで設定するアイテム名を入力してください");
            player.sendMessage("[itemfix] §dスニーク(SHIFTキー)で編集をキャンセルします。");
            player.closeInventory();
            Consumer<String> written = name -> {
                var meta = item.getItemMeta();
                if (meta != null) {
                    meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
                    item.setItemMeta(meta);
                }
            };
            EventListener.registerInternal(player, written, () -> openGui(player, item));
        }
    }

    private static void onEditLocNameClicked(Player player, ItemStack item) {
        var event = new ItemFixEnterSetLocNameEvent(player, item);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            player.playSound(player, Sound.ENTITY_CAT_AMBIENT, 10, 2);
            player.sendMessage("[itemfix] §dチャットで設定するローカライズ名を入力してください");
            player.sendMessage("[itemfix] §dスニーク(SHIFTキー)で編集をキャンセルします。");
            player.closeInventory();
            Consumer<String> written = name -> {
                var meta = item.getItemMeta();
                if (meta != null) {
                    meta.setLocalizedName(ChatColor.translateAlternateColorCodes('&', name));
                    item.setItemMeta(meta);
                }
            };
            EventListener.registerInternal(player, written, () -> openGui(player, item));
        }
    }

    private static void onEditLoreClicked(Player player, ItemStack item) {
        player.playSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
        ItemFixLore.openGui(player, item);
    }

    private static void onEditHideFlagClicked(Player player, ItemStack item) {
        player.playSound(player, Sound.BLOCK_LEVER_CLICK, 10, 0.5f);
        ItemFixHideFlag.openGui(player, item);
    }

    private static void onEditEnchantmentClicked(Player player, ItemStack item) {
        player.playSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
        ItemFixEnchantment.openGui(player, item);
    }

    private static void onEditUnbreakableClicked(Player player, ItemStack item) {
        var meta = Optional.ofNullable(item.getItemMeta());
        player.playSound(player, Sound.BLOCK_ENCHANTMENT_TABLE_USE, 10, 2);
        meta.ifPresent(x -> x.setUnbreakable(!x.isUnbreakable()));
        meta.ifPresent(item::setItemMeta);
        openGui(player, item);
    }

    private static void onEditModelDataClicked(InventoryClickEvent event, Player player, ItemStack item) {
        var meta = Optional.ofNullable(item.getItemMeta());
        var model = meta.filter(ItemMeta::hasCustomModelData).map(ItemMeta::getCustomModelData);
        var data = switch (event.getClick()) {
            case LEFT, SHIFT_LEFT -> model.map(v -> event.isShiftClick() ? v + 10 : v + 1).orElse(0);
            case RIGHT, SHIFT_RIGHT -> model.map(v -> event.isShiftClick() ? v - 10 : v - 1).orElse(0);
            case DROP, CONTROL_DROP -> null;
            default -> Integer.MIN_VALUE;
        };
        if (Objects.equals(Integer.MIN_VALUE, data)) {
            return;
        }
        var pitch = switch (event.getClick()) {
            case LEFT, SHIFT_LEFT -> 1;
            case RIGHT, SHIFT_RIGHT -> 0.5f;
            default -> 2;
        };
        player.playSound(player, Sound.BLOCK_LEVER_CLICK, 10, pitch);
        meta.ifPresent(x -> x.setCustomModelData(data));
        meta.ifPresent(item::setItemMeta);
        openGui(player, item);
    }

    private static void onEditTagClicked(Player player, ItemStack item) {
        player.playSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
        ItemFixTag.openGui(player, new TagItem(item), new Stack<>());
    }
}
