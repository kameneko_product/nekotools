package kame.nekotools.inventory.itemfix.gui;

import kame.nekocore.inventory.ActionInventory;
import kame.nekocore.inventory.ActionItemStack;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Objects;

public class ItemFixHideFlag {


    public static void openGui(Player player, ItemStack item) {
        var inv = new ActionInventory("ItemFix ItemFlag Editor", 27);
        var meta = Objects.requireNonNull(item.getItemMeta());
        var items = Arrays.stream(ItemFlag.values()).map(x -> create(player, item, x)).toArray(ItemStack[]::new);
        for (int i = 0; i < items.length; i++) {
            inv.setItem(i * 2, items[i]);
        }
        var addall = new ActionItemStack(Material.MUSIC_DISC_CAT, "§a§l全て付与");
        addall.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
        addall.addClickEvent(x -> meta.addItemFlags(ItemFlag.values()));
        addall.addClickEvent(x -> item.setItemMeta(meta));
        addall.addClickEvent(x -> openGui(player, item));
        inv.setItem(18, addall);
        var remall = new ActionItemStack(Material.MUSIC_DISC_CHIRP, "§4§l全て解除");
        remall.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 1);
        remall.addClickEvent(x -> meta.removeItemFlags(ItemFlag.values()));
        remall.addClickEvent(x -> item.setItemMeta(meta));
        remall.addClickEvent(x -> openGui(player, item));
        inv.setItem(19, remall);
        var back = new ActionItemStack(Material.BARRIER, "§4§l戻る");
        back.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
        back.addClickEvent(x -> ItemFix.openGui(player, item));
        inv.setItem(26, back);
        inv.openInventory(player);
    }

    private static ActionItemStack create(Player player, ItemStack item, ItemFlag flag) {
        var meta = Objects.requireNonNull(item.getItemMeta());
        var hasFlag = meta.hasItemFlag(flag);
        var name = String.format("§e%s §f(%s§f)", flag, hasFlag ? "§aON" : "§cOFF");
        var action = new ActionItemStack(getMaterialFromItemFlag(player, flag), name);
        if (hasFlag) {
            action.setGlow();
            action.addClickEvent(x -> meta.removeItemFlags(flag));
            action.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 1);
        } else {
            action.addClickEvent(x -> meta.addItemFlags(flag));
            action.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
        }
        action.addClickEvent(x -> item.setItemMeta(meta));
        action.addClickEvent(x -> openGui(player, item));
        return action;
    }

    private static Material getMaterialFromItemFlag(Player player, ItemFlag flag) {
        try {
            return switch (flag) {
                case HIDE_ENCHANTS -> Material.BOOK;
                case HIDE_ATTRIBUTES -> Material.DIAMOND_SWORD;
                case HIDE_UNBREAKABLE -> Material.ANVIL;
                case HIDE_DESTROYS -> Material.ITEM_FRAME;
                case HIDE_PLACED_ON -> Material.GRASS_BLOCK;
                case HIDE_POTION_EFFECTS -> Material.POTION;
                case HIDE_DYE -> Material.LIME_DYE;
                case HIDE_ARMOR_TRIM -> Material.LEATHER_CHESTPLATE;
            };
        } catch (Throwable e) {
            var error = new TextComponent("§c[NekoTools] Error:§k------");
            error.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(e.toString())));
            player.spigot().sendMessage(error);
            player.sendMessage("§c[NekoTools] §rItemFlagに未対応の物がありました:§c" + flag);
            e.printStackTrace();
            return Material.BARRIER;
        }
    }
}
