package kame.nekotools.inventory.itemfix.gui;

import com.google.common.collect.Sets;
import kame.nekocore.inventory.ActionInventory;
import kame.nekocore.inventory.ActionItemStack;
import kame.nekotools.inventory.itemfix.events.ItemFixEnterAddLoreEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class ItemFixLore {

    public static void openGui(Player player, ItemStack item) {
        openGui(player, item, Sets.newHashSet(), 1);
    }

    public static void openGui(Player player, ItemStack item, Set<Integer> selected, int page) {
        var inv = new ActionInventory("ItemFix Lore Editor", 54);
        var meta = Objects.requireNonNull(item.getItemMeta());
        var lore = meta.getLore() != null ? meta.getLore() : new ArrayList<String>();
        List<ActionItemStack> items = new ArrayList<>();
        for (var i = 0; i < lore.size(); i++) {
            items.add(create(player, item, selected, i, lore.get(i), page));
        }
        var sub = items.subList(Math.min((page - 1) * 45, items.size()), Math.min(page * 45, items.size()));
        IntStream.range(0, sub.size()).forEach(x -> inv.setItem(x, sub.get(x)));
        setPrevNext(player, item, inv, selected, items.size(), page);
        setFunctionKey(player, item, inv, selected, page);
        var back = new ActionItemStack(Material.BARRIER, "§4§l戻る");
        back.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
        back.addClickEvent(x -> ItemFix.openGui(player, item));
        inv.setItem(inv.getSize() - 1, back);
        inv.openInventory(player);
    }

    private static ActionItemStack create(Player player, ItemStack item, Set<Integer> selected, int index, String lore, int page) {
        var action = new ActionItemStack(Material.FILLED_MAP, "§f" + lore);
        action.setGlow(selected.contains(index));
        action.addClickEvent(x -> selected.add(index), x -> !selected.remove(index));
        action.addClickSound(player, Sound.ENTITY_ITEM_FRAME_ADD_ITEM, 10, 2, x -> selected.contains(index));
        action.addClickSound(player, Sound.ENTITY_ITEM_FRAME_REMOVE_ITEM, 10, 2, x -> !selected.contains(index));
        action.addClickEvent(x -> openGui(player, item, selected, page));
        return action;
    }

    private static void setPrevNext(Player player, ItemStack item, ActionInventory inv, Set<Integer> selected, int size, int page) {
        if (page > 1) {
            var prev = new ActionItemStack(Material.LANTERN, "§l§d前ページ");
            prev.addClickEvent(x -> openGui(player, item, selected, page - 1));
            prev.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
            inv.setItem(45, prev);
        }
        if (page * 45 < size) {
            var next = new ActionItemStack(Material.SOUL_LANTERN, "§l§a次ページ");
            next.addClickEvent(x -> openGui(player, item, selected, page + 1));
            next.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
            inv.setItem(46, next);
        }
    }

    private static void setFunctionKey(Player player, ItemStack item, ActionInventory inv, Set<Integer> selected, int page) {
        if (!selected.isEmpty()) {
            var clr = new ActionItemStack(Material.STRUCTURE_VOID, "§l§6選択解除");
            clr.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
            clr.addClickEvent(x -> openGui(player, item, new HashSet<>(), page));
            inv.setItem(47, clr);
            var rem = new ActionItemStack(Material.MUSIC_DISC_CHIRP, "§l§4削除§f(§eSHIFTクリック§f)");
            rem.addClickEvent(x -> {
                if (!x.isShiftClick()) {
                    player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                    return;
                }
                player.playSound(player, Sound.ENTITY_ITEM_BREAK, 10, 2);
                var meta = Objects.requireNonNull(item.getItemMeta());
                var lore = meta.getLore() != null ? meta.getLore() : new ArrayList<String>();
                selected.stream().sorted((a, b) -> b - a).forEach(k -> lore.remove((int)k));
                meta.setLore(lore);
                item.setItemMeta(meta);
                openGui(player, item, new HashSet<>(), page);
            });
            inv.setItem(51, rem);
        }
        if (selected.isEmpty()) {
            int size = Optional.ofNullable(item.getItemMeta()).map(ItemMeta::getLore).map(List::size).orElse(0);
            var add = new ActionItemStack(Material.MUSIC_DISC_CAT, "§l§a追加");
            add.addClickEvent(x -> onClicked(player, item, size));
            inv.setItem(50, add);
        } else if (selected.size() == 1) {
            var add = new ActionItemStack(Material.MUSIC_DISC_WAIT, "§l§b挿入");
            add.addClickEvent(x -> onClicked(player, item, selected.iterator().next()));
            inv.setItem(50, add);
        }
        if (selected.size() == 1) {
            var meta = Objects.requireNonNull(item.getItemMeta());
            var lore = Optional.ofNullable(meta.getLore()).orElse(new ArrayList<>());
            var mov = new ActionItemStack(Material.SUNFLOWER, "§l§b順序変更", "§c左クリック§6:§c前へ", "§a右クリック§6:§a後へ");
            mov.addClickEvent(x -> {
                int select = selected.iterator().next();
                var move = x.isLeftClick() ? -1 : x.isRightClick() ? 1 : 0;
                if (select + move < 0 || select + move >= lore.size()) {
                    player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                } else {
                    lore.add(select + move, lore.remove(select));
                    player.playSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
                    meta.setLore(lore);
                    item.setItemMeta(meta);
                    openGui(player, item, Sets.newHashSet(select + move), page);
                }
            });
            inv.setItem(49, mov);
        }
    }

    private static void onClicked(Player player, ItemStack item, int index) {
        var event = new ItemFixEnterAddLoreEvent(player, item, index);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            player.playSound(player, Sound.ENTITY_CAT_AMBIENT, 10, 2);
            player.sendMessage("[itemfix] §dチャットで追加する説明文を入力してください('&'+0-f,i,k,l,m,u,o,rは装飾文字)");
            player.closeInventory();
            Consumer<String> written = line -> {
                var meta = item.getItemMeta();
                if (meta != null) {
                    List<String> lore = meta.getLore() != null ? meta.getLore() : new ArrayList<>();
                    lore.add(index, ChatColor.translateAlternateColorCodes('&', line));
                    meta.setLore(lore);
                    item.setItemMeta(meta);
                }
            };
            EventListener.registerInternal(player, written, () -> openGui(player, item));
        }
    }
}
