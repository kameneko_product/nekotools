package kame.nekotools.inventory.itemfix.gui;

import com.google.common.collect.Sets;
import kame.nekocore.inventory.ActionInventory;
import kame.nekocore.inventory.ActionItemStack;
import kame.nekocore.inventory.ClickEvent;
import kame.nekotools.NekoTools;
import kame.nekotools.inventory.itemfix.events.ItemFixEnterEditTagEvent;
import kame.nekotools.inventory.itemfix.events.ItemFixEnterEditTagNameEvent;
import kame.tagapi.*;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ItemFixTag {

    private static final Map<UUID, Map<?, TagBase>> clipboard = new HashMap<>();

    private static final Set<UUID> closing = new HashSet<>();
    private static final Set<UUID> changed = new HashSet<>();

    private static final Map<Class<? extends TagBase>, Material> types = new HashMap<>() {{
        put(TagSection.class, Material.BOOKSHELF);
        put(TagList.class, Material.GLASS);
        put(TagByteArray.class, Material.RED_STAINED_GLASS);
        put(TagIntArray.class, Material.GREEN_STAINED_GLASS);
        put(TagLongArray.class, Material.YELLOW_STAINED_GLASS);

        put(TagByte.class, Material.ORANGE_DYE);
        put(TagShort.class, Material.MAGENTA_DYE);
        put(TagInt.class, Material.LIGHT_BLUE_DYE);
        put(TagLong.class, Material.YELLOW_DYE);
        put(TagFloat.class, Material.LIME_DYE);
        put(TagDouble.class, Material.PINK_DYE);
        put(TagString.class, Material.OAK_SIGN);
    }};

    public static TagNode get(TagSection section, Iterator<String> paths) {
        if (paths.hasNext()) {
            var key = paths.next();
            return get(section.get(key), paths) instanceof TagNode node ? node : section;
        } else {
            return section;
        }
    }

    public static TagNode get(TagList list, Iterator<String> paths) {
        if (paths.hasNext()) {
            var index = parseInt(paths.next());
            return get(list.get(index), paths) instanceof TagNode node ? node : list;
        } else {
            return list;
        }
    }

    public static TagBase get(TagBase base, Iterator<String> paths) {
        if (base instanceof TagSection section) {
            return get(section, paths);
        } else if (base instanceof TagList list) {
            return get(list, paths);
        } else {
            paths.remove();
            return null;
        }
    }

    private static int parseInt(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static void openGui(Player player, TagItem item, Stack<String> paths) {
        openGui(player, item, paths, 1);
    }

    public static void openGui(Player player, TagItem item, Stack<String> paths, int page) {
        var node = get(item.getSection(), paths.iterator());
        openGui(player, item, paths, node, new HashSet<>(), page);
    }

    public static void openGui(Player player, TagItem item, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        var inv = createGui(player, item, paths, node, selected, page);
        closing.add(player.getUniqueId());
        inv.addCloseEvent(x -> onClose(player, item, paths, node, selected, page));
        inv.openInventory(player);
        closing.remove(player.getUniqueId());
    }

    private static void onClose(Player player, TagItem item, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        if (closing.remove(player.getUniqueId())) {
            return;
        }
        if (!changed.contains(player.getUniqueId())) {
            return;
        }
        Bukkit.getScheduler().runTask(JavaPlugin.getPlugin(NekoTools.class), () -> {
            var confirm = new ActionInventory("§l編集内容を破棄して閉じますか?", 9);
            confirm.setItem(2, new ActionItemStack(Material.CAMPFIRE, "§4§l破棄して閉じる") {{
                addClickEvent(x -> changed.remove(player.getUniqueId()));
                addClickEvent(x -> player.closeInventory());
                addClickSound(player, Sound.ITEM_BOOK_PUT, 10, 2);
            }});
            confirm.setItem(6, new ActionItemStack(Material.SOUL_CAMPFIRE, "§6§l編集を続ける") {{
                addClickEvent(x -> openGui(player, item, paths, node, selected, page));
                addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
            }});
            confirm.openInventory(player);
            player.playSound(player, Sound.BLOCK_LAVA_POP, 10, 2);
        });
    }

    public static ActionInventory createGui(Player player, TagItem item, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        if (node instanceof TagSection section) {
            return createGui(player, item, paths, section, selected, page);
        } else if (node instanceof TagList list) {
            return createGui(player, item, paths, list, selected, page);
        } else {
            return null;
        }
    }

    public static ActionInventory createGui(Player player, TagItem item, Stack<String> paths, TagSection node, Set<Object> selected, int page) {
        var title = paths.empty() ? "ItemFix GUI Editor" : String.join("§2/§r", paths);
        title += node.size() > 44 || page > 1 ? " §5(" + page + "/" + (node.size() / 45 + 1) + ")" : "";
        title += changed.contains(player.getUniqueId()) ? "§0§l*" : "";
        var inv = new ActionInventory(title, 54);
        List<ActionItemStack> items = new ArrayList<>();
        for (var entry : node.entrySet()) {
            var type = types.get(entry.getValue().getClass());
            var action = new ActionItemStack(type);
            action.addClickEvent(onClick(entry.getKey(), entry.getValue(), player, item, paths, node, selected, page));
            action.setGlow(selected.contains(entry.getKey()));
            action.setDisplayName("§a%s§d(§f%s§d)".formatted(entry.getKey(), getFormatName(entry.getValue())));
            action.setLore(getFormatLore(entry.getValue()));
            items.add(action);
        }
        var sub = items.subList((page - 1) * 45, Math.min(page * 45, items.size()));
        IntStream.range(0, sub.size()).forEach(x -> inv.setItem(x, sub.get(x)));
        setPrevNext(player, item, inv, paths, node, selected, items.size(), page);
        setFunctionKey(player, item, inv, paths, node, selected, page);
        setClipboard(player, item, inv, paths, node, selected, page);
        setSaveBack(player, item, inv, paths, node, selected, page);
        return inv;
    }

    public static ActionInventory createGui(Player player, TagItem item, Stack<String> paths, TagList node, Set<Object> selected, int page) {
        var title = paths.empty() ? "ItemFix GUI Editor" : String.join("§2/§r", paths);
        title += node.size() > 44 || page > 1 ? " §5(" + page + "/" + (node.size() / 45 + 1) + ")" : "";
        title += changed.contains(player.getUniqueId()) ? "§0§l*" : "";
        var inv = new ActionInventory(title, 54);
        List<ActionItemStack> items = new ArrayList<>();
        for (var index : IntStream.range(0, node.size()).toArray()) {
            var tag = node.get(index);
            if (tag == null) {
                items.add(null);
                continue;
            }
            var type = types.get(tag.getClass());
            var action = new ActionItemStack(type);
            action.addClickEvent(onClick(index, tag, player, item, paths, node, selected, page));
            action.setGlow(selected.contains(index));
            action.setDisplayName("§4%s§d(§f%s§d)".formatted(index, getFormatName(tag)));
            action.setLore(getFormatLore(tag));
            items.add(action);
        }
        var sub = items.subList(Math.min((page - 1) * 45, items.size()), Math.min(page * 45, items.size()));
        IntStream.range(0, sub.size()).forEach(x -> inv.setItem(x, sub.get(x)));
        setPrevNext(player, item, inv, paths, node, selected, items.size(), page);
        setFunctionKey(player, item, inv, paths, node, selected, page);
        setClipboard(player, item, inv, paths, node, selected, page);
        setSaveBack(player, item, inv, paths, node, selected, page);
        return inv;
    }

    private static ClickEvent onClick(Object element, TagBase tag, Player player, TagItem item, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        return x -> {
            if (x.isShiftClick() || !selected.isEmpty()) {
                if (!selected.remove(element)) {
                    selected.add(element);
                    player.playSound(player, Sound.ENTITY_ITEM_FRAME_ADD_ITEM, 10, 2);
                } else {
                    player.playSound(player, Sound.ENTITY_ITEM_FRAME_REMOVE_ITEM, 10, 2);
                }
                openGui(player, item, paths, node, selected, page);
            } else if (tag instanceof TagNode) {
                player.playSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
                paths.push(element.toString());
                openGui(player, item, paths);
            }
        };
    }

    private static void setPrevNext(Player player, TagItem item, ActionInventory inv, Stack<String> paths, TagNode node, Set<Object> selected, int size, int page) {
        if (page > 1) {
            var prev = new ActionItemStack(Material.LANTERN, "§l§d前ページ");
            prev.addClickEvent(x -> openGui(player, item, paths, node, selected, page - 1));
            prev.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
            inv.setItem(45, prev);
        }
        if (page * 45 < size) {
            var next = new ActionItemStack(Material.SOUL_LANTERN, "§l§a次ページ");
            next.addClickEvent(x -> openGui(player, item, paths, node, selected, page + 1));
            next.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
            inv.setItem(46, next);
        }
    }

    private static void setFunctionKey(Player player, TagItem item, ActionInventory inv, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        if (!selected.isEmpty()) {
            var clr = new ActionItemStack(Material.STRUCTURE_VOID, "§l§6選択解除");
            clr.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
            clr.addClickEvent(x -> openGui(player, item, paths, node, new HashSet<>(), page));
            inv.setItem(47, clr);
            var rem = new ActionItemStack(Material.MUSIC_DISC_CHIRP, "§l§4削除§f(§eSHIFTクリック§f)");
            rem.addClickEvent(x -> {
                if (!x.isShiftClick()) {
                    player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                    return;
                }
                player.playSound(player, Sound.ENTITY_ITEM_BREAK, 10, 2);
                if (node instanceof TagSection section) {
                    selected.forEach(k -> section.remove((String)k));
                }
                if (node instanceof TagList list) {
                    selected.stream().sorted((a, b) -> (int)b - (int)a).forEach(k -> list.remove((int)k));
                }
                changed.add(player.getUniqueId());
                openGui(player, item, paths, node, new HashSet<>(), page);
            });
            inv.setItem(51, rem);
        }

        if (selected.size() == 1) {
            var out = new ActionItemStack(Material.NAME_TAG, "§l§6文字出力");
            out.addClickSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
            out.addClickEvent(x -> {
                var text = new TextComponent("[itemfix] §d§l§nクリックで出力内容をクリップボードへコピーします");
                if (node instanceof TagList data) {
                    var action = net.md_5.bungee.api.chat.ClickEvent.Action.COPY_TO_CLIPBOARD;
                    var select = selected.stream().mapToInt(y -> (int)y).findFirst().orElse(0);
                    var output = Optional.ofNullable(data.get(select)).map(TagBase::getNBTString).orElse("");
                    text.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(action, output));
                } else if (node instanceof TagSection data) {
                    var action = net.md_5.bungee.api.chat.ClickEvent.Action.COPY_TO_CLIPBOARD;
                    var select = selected.stream().map(y -> (String)y).findFirst().orElse("");
                    var output = Optional.ofNullable(data.get(select)).map(TagBase::getNBTString).orElse("");
                    text.setClickEvent(new net.md_5.bungee.api.chat.ClickEvent(action, output));
                }
                player.spigot().sendMessage(text);
            });
            inv.setItem(49, out);
        }
        if (selected.isEmpty()) {
            var add = new ActionItemStack(Material.MUSIC_DISC_CAT, "§l§a追加");
            add.addClickEvent(x -> onAddDataClicked(player, item, node, paths, page));
            inv.setItem(50, add);
        } else if (selected.size() == 1 && node instanceof TagList list) {
            var mov = new ActionItemStack(Material.SUNFLOWER, "§l§b順序変更", "§c左クリック§6:§c前へ", "§a右クリック§6:§a後へ");
            mov.addClickEvent(x -> {
                var select = selected.stream().mapToInt(y -> (int)y).findFirst().orElse(0);
                var move = x.isLeftClick() ? -1 : x.isRightClick() ? 1 : 0;
                if (select + move < 0 || select + move >= list.size()) {
                    player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                } else {
                    list.add(select + move, list.remove(select));
                    changed.add(player.getUniqueId());
                    player.playSound(player, Sound.BLOCK_LEVER_CLICK, 10, 2);
                    openGui(player, item, paths, node, Sets.newHashSet(select + move), page);
                }
            });
            inv.setItem(50, mov);
        } else if (selected.size() == 1 && node instanceof TagSection section) {
            var edt = new ActionItemStack(Material.SUNFLOWER, "§l§bキー変更");
            var name = selected.stream().map(x -> (String)x).findFirst().orElse("");
            edt.addClickEvent(x -> onEditNameClicked(player, item, section, paths, name, page));
            inv.setItem(50, edt);
        }
    }

    private static void setClipboard(Player player, TagItem item, ActionInventory inv, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        Function<TagBase, TagBase> clone = base -> {
            try {
                return TagBase.parse(base.toString());
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
        if (clipboard.containsKey(player.getUniqueId())) {
            var clip = clipboard.get(player.getUniqueId());
            var lore = clip.entrySet().stream().map(x -> "§b" + x.getKey() + "§6:§f" + getString(x.getValue())).toArray(String[]::new);
            var map = clipboard.get(player.getUniqueId());
            if (node instanceof TagSection section) {
                var paste = new ActionItemStack(Material.FILLED_MAP, "§l§b貼り付け", lore);
                paste.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
                paste.addClickEvent(x -> map.forEach((k, v) -> section.put(Objects.toString(k), clone.apply(v))));
                paste.addClickEvent(x -> changed.add(player.getUniqueId()));
                paste.addClickEvent(x -> openGui(player, item, paths, node, new HashSet<>(), page));
                inv.setItem(48, paste);
            } else if (node instanceof TagList list && map.values().stream().allMatch(list.getElementType()::isInstance)) {
                var paste = new ActionItemStack(Material.FILLED_MAP, "§l§b貼り付け", lore);
                paste.addClickSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
                paste.addClickEvent(x -> map.forEach((k, v) -> list.add(clone.apply(v))));
                paste.addClickEvent(x -> changed.add(player.getUniqueId()));
                paste.addClickEvent(x -> openGui(player, item, paths, node, new HashSet<>(), page));
                inv.setItem(48, paste);
            } else {
                var paste = new ActionItemStack(Material.FILLED_MAP, "§l§b貼り付け §f(§cデータ型不一致§f)", lore);
                paste.addClickSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1);
                inv.setItem(48, paste);
            }
        }
        if (!selected.isEmpty()) {
            var copy = new ActionItemStack(Material.MAP, "§l§aコピー");
            copy.addClickEvent(x -> {
                player.playSound(player, Sound.ITEM_BOOK_PAGE_TURN, 10, 2);
                if (node instanceof TagSection section) {
                    var data = section.entrySet()
                            .stream()
                            .filter(entry -> selected.contains(entry.getKey()))
                            .collect(Collectors.toMap(Map.Entry::getKey, entry -> clone.apply(entry.getValue())));
                    clipboard.put(player.getUniqueId(), data);
                }
                if (node instanceof TagList list) {
                    Function<Integer, TagBase> copyValue = index -> clone.apply(list.get(index));
                    var data = IntStream.range(0, list.size())
                            .boxed()
                            .filter(selected::contains)
                            .collect(Collectors.toMap(Objects::toString, copyValue));
                    clipboard.put(player.getUniqueId(), data);
                }
                openGui(player, item, paths, node, new HashSet<>(), page);
            });
            inv.setItem(48, copy);
        }
    }

    private static void setSaveBack(Player player, TagItem item, ActionInventory inv, Stack<String> paths, TagNode node, Set<Object> selected, int page) {
        if (changed.contains(player.getUniqueId())) {
            var save = new ActionItemStack(Material.WRITABLE_BOOK, "§l§6保存");
            save.addClickEvent(x -> item.save());
            save.addClickEvent(x -> changed.remove(player.getUniqueId()));
            save.addClickEvent(x -> openGui(player, item, paths, node, selected, page));
            save.addClickSound(player, Sound.BLOCK_ENCHANTMENT_TABLE_USE, 10, 2);
            inv.setItem(52, save);
        }
        var close = new ActionItemStack(Material.BARRIER);
        if (paths.isEmpty() && changed.contains(player.getUniqueId())) {
            close.addClickSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 1, x -> !x.isShiftClick());
            close.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2, InventoryClickEvent::isShiftClick);
            close.addClickEvent(x -> closing.add(player.getUniqueId()), InventoryClickEvent::isShiftClick);
            close.addClickEvent(x -> changed.remove(player.getUniqueId()), InventoryClickEvent::isShiftClick);
            close.addClickEvent(x -> ItemFix.openGui(player, item.getItemStack()), InventoryClickEvent::isShiftClick);
            inv.setMetaItem(53, close, "§l§c戻る§f(§eSHIFTクリック§f)", "§e変更を破棄してメニューへ");
        } else if (paths.isEmpty()) {
            close.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
            close.addClickEvent(x -> closing.add(player.getUniqueId()));
            close.addClickEvent(x -> changed.remove(player.getUniqueId()));
            close.addClickEvent(x -> ItemFix.openGui(player, item.getItemStack()));
            inv.setMetaItem(53, close, "§l§c戻る", "§eメニューへ戻る");
        } else {
            close.addClickSound(player, Sound.ITEM_ARMOR_EQUIP_LEATHER, 10, 2);
            close.addClickEvent(x -> paths.pop());
            close.addClickEvent(x -> openGui(player, item, paths));
            inv.setMetaItem(53, close, "§l§c戻る");
        }
    }

    private static void onAddDataClicked(Player player, TagItem item, TagNode node, Stack<String> paths, int page) {
        var event = new ItemFixEnterEditTagEvent(player, item, node, paths, page);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            player.playSound(player, Sound.ENTITY_CAT_AMBIENT, 10, 2);
            player.sendMessage("[itemfix] §dチャットで追加するデータを 入力してください");
            closing.add(player.getUniqueId());
            player.closeInventory();
            Consumer<String> written = data ->
            {
                try {
                    var args = data.split(":", 2);
                    if (node instanceof TagSection tag) {
                        tag.put(args[0], TagBase.parse(args[1]));
                    }
                    if (node instanceof TagList tag) {
                        if (args.length >= 2) {
                            tag.add(Integer.parseInt(args[0]), TagBase.parse(args[1]));
                        } else {
                            tag.add(TagBase.parse(args[0]));
                        }
                    }
                    changed.add(player.getUniqueId());
                } catch (Exception e) {
                    player.playSound(player, Sound.BLOCK_NOTE_BLOCK_BASS, 10, 2);
                    player.playSound(player, Sound.BLOCK_ANVIL_BREAK, 10, 2);
                    player.sendMessage("[itemfix] §c入力されたデータをNBTに変換できませんでした");
                }
            };
            EventListener.registerInternal(player, written, () -> ItemFixTag.openGui(player, item, paths, node, Sets.newHashSet(), page));
        }
    }

    private static void onEditNameClicked(Player player, TagItem item, TagSection section, Stack<String> paths, String name, int page) {
        var event = new ItemFixEnterEditTagNameEvent(player, item, section, paths, name, page);
        Bukkit.getPluginManager().callEvent(event);
        if (!event.isCancelled()) {
            player.playSound(player, Sound.ENTITY_CAT_AMBIENT, 10, 2);
            player.sendMessage("[itemfix] §dチャットで変更後のキー名を入力してください");
            closing.add(player.getUniqueId());
            player.closeInventory();
            var select = new String[]{name};
            Consumer<String> written = key -> {
                section.put(select[0] = key, section.remove(name));
                changed.add(player.getUniqueId());
            };
            EventListener.registerInternal(player, written, () -> ItemFixTag.openGui(player, item, paths, section, Sets.newHashSet(select[0]), page));
        }
    }

    private static String getFormatName(TagBase data) {
        return data instanceof TagSection          /**/ ? "Section"
                : data instanceof TagByte          /**/ ? "Byte"
                : data instanceof TagShort         /**/ ? "Short"
                : data instanceof TagInt           /**/ ? "Integer"
                : data instanceof TagLong          /**/ ? "Long"
                : data instanceof TagFloat         /**/ ? "Float"
                : data instanceof TagDouble        /**/ ? "Double"
                : data instanceof TagString        /**/ ? "String"
                : data instanceof TagByteArray tag /**/ ? "Byte§f[§b" + tag.data().length + "§f]"
                : data instanceof TagIntArray tag  /**/ ? "Int§f[§b" + tag.data().length + "§f]"
                : data instanceof TagLongArray tag /**/ ? "Long§f[§b" + tag.data().length + "§f]"
                : data instanceof TagList tag      /**/ ? getFormatName(tag) : "ERROR";
    }

    private static String getFormatName(TagList data) {
        var type = data.getElementType();
        var inner
                = type == TagSection.class   /**/ ? "Section"
                : type == TagList.class      /**/ ? "List"
                : type == TagByte.class      /**/ ? "Byte"
                : type == TagShort.class     /**/ ? "Short"
                : type == TagInt.class       /**/ ? "Int"
                : type == TagLong.class      /**/ ? "Long"
                : type == TagFloat.class     /**/ ? "Float"
                : type == TagDouble.class    /**/ ? "Double"
                : type == TagString.class    /**/ ? "String"
                : type == TagByteArray.class /**/ ? "Byte§f[§0?§f]"
                : type == TagIntArray.class  /**/ ? "Int§f[§0?§f]"
                : type == TagLongArray.class /**/ ? "Long§f[§0?§f]" : "ERROR";
        return "List§6<§a" + inner + "§6>[§b" + data.size() + "§6]";
    }

    private static List<String> getFormatLore(TagBase data) {
        return data instanceof TagSection tag      /**/ ? tag.entrySet().stream().map(x -> "§b" + x.getKey() + "§6: " + getString(x.getValue())).toList()
                : data instanceof TagList tag      /**/ ? IntStream.range(0, tag.size()).mapToObj(x -> "§b" + x + "§6: " + getString(tag.get(x))).toList()
                : data instanceof TagByte tag      /**/ ? Stream.of("§f" + tag.data() + "§6b").toList()
                : data instanceof TagShort tag     /**/ ? Stream.of("§f" + tag.data() + "§6s").toList()
                : data instanceof TagInt tag       /**/ ? Stream.of("§f" + tag.data() + "§6 ").toList()
                : data instanceof TagLong tag      /**/ ? Stream.of("§f" + tag.data() + "§6l").toList()
                : data instanceof TagFloat tag     /**/ ? Stream.of("§f" + tag.data() + "§6f").toList()
                : data instanceof TagDouble tag    /**/ ? Stream.of("§f" + tag.data() + "§6d").toList()
                : data instanceof TagString tag    /**/ ? Stream.of("§e\"§f" + getString(tag) + "§e\"").toList()
                : data instanceof TagByteArray tag /**/ ? Stream.of("§f[§6B§6;§f" + join(tag) + "]").toList()
                : data instanceof TagIntArray tag  /**/ ? Stream.of("§f[§bI§6;§f" + join(tag) + "]").toList()
                : data instanceof TagLongArray tag /**/ ? Stream.of("§f[§eL§6;§f" + join(tag) + "]").toList()
                : Collections.emptyList();
    }

    private static String join(TagArray<?> array) {
        return Stream.of(array).map(Objects::toString).collect(Collectors.joining(","));
    }

    private static String getString(@Nullable TagBase data) {
        var ret = "§f" + Optional.ofNullable(data).map(TagBase::getNBTString).orElse("(null)");
        return ret.length() < 55 ? ret : ret.substring(0, 55).concat("§5... ");
    }
}
