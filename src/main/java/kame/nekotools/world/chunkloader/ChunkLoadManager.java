package kame.nekotools.world.chunkloader;

import kame.nekotools.NekoTools;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemDisplay;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ChunkLoadManager {

    private static final Set<Chunk> chunks = new HashSet<>();
    private static final String ChunkLoaderTag = "NekoTools.ChunkLoader";
    private static final ItemStack item;

    static {
        item = new ItemStack(Material.DIAMOND_BLOCK);
        var im = Objects.requireNonNull(item.getItemMeta());
        im.setDisplayName("§6ChunkLoader");
        im.setLore(List.of("§b読み込ませたいチャンクに設置"));
        item.setItemMeta(im);
    }

    public static void initialize() {
        var folder = JavaPlugin.getPlugin(NekoTools.class).getDataFolder();
        var config = YamlConfiguration.loadConfiguration(new File(folder, "chunks.save"));
        var section = config.getConfigurationSection("chunks");
        if (section != null) {
            for (String worldId : section.getKeys(true)) {
                World world = Bukkit.getWorld(UUID.fromString(worldId));
                if (world == null) continue;
                for (String num : section.getStringList(worldId)) {
                    String[] args = num.split(" ");
                    chunks.add(world.getChunkAt(Integer.parseInt(args[0]), Integer.parseInt(args[1])));
                }
            }
            for (Chunk chunk : chunks) {
                if (!chunk.isLoaded()) chunk.load();
                chunk.setForceLoaded(true);
            }
        } else {
            saveConfig();
        }
    }

    private static void saveConfig() {
        var config = new YamlConfiguration();
        var group = chunks.stream().collect(Collectors.groupingBy(Chunk::getWorld));
        for (var entry : group.entrySet()) {
            var chunks = entry.getValue().stream().map(x -> String.format("%s %s", x.getX(), x.getZ())).toList();
            config.set(String.format("chunks.%s", entry.getKey().getUID()), chunks);
        }
        try {
            var folder = JavaPlugin.getPlugin(NekoTools.class).getDataFolder();
            config.save(new File(folder, "chunks.save"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean isLoaded(Chunk chunk) {
        return chunks.contains(chunk);
    }

    public static ItemStack getItem() {
        return item.clone();
    }

    public static ChunkLoader createNew(Block block) {
        var loader = ChunkLoader.create(block, ChunkLoadManager::initializeEntity);
        var chunk = loader.getChunk();
        chunks.add(chunk);
        loader.setForceLoaded(true);
        loader.getWorld().playSound(loader.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1, 1.5f);
        saveConfig();
        return loader;
    }

    private static void initializeEntity(ItemDisplay display) {
        display.setItemStack(new ItemStack(Material.BEACON));
        display.setItemDisplayTransform(ItemDisplay.ItemDisplayTransform.FIXED);
        display.addScoreboardTag(ChunkLoaderTag);
    }

    public static void delete(ChunkLoader loader) {
        chunks.remove(loader.getChunk());
        loader.setForceLoaded(false);
        loader.getWorld().playSound(loader.getLocation(), Sound.BLOCK_BEACON_DEACTIVATE, 1, 1.5f);
        loader.destroy();
        saveConfig();
    }

    public static Optional<ChunkLoader> findFromBlock(Block block) {
        if (block == null) return Optional.empty();
        var loc = block.getLocation().add(0.5, 1, 0.5);
        var entities = block.getWorld().getNearbyEntities(loc, 0.1, 0.1, 0.1, ChunkLoadManager::isChunkLoaderEntity);
        return entities.stream().findFirst().map(x -> new ChunkLoader(block, x));
    }

    public static Optional<ChunkLoader> findFromChunk(Chunk chunk) {
        if (chunk == null) return Optional.empty();
        var centerY = chunk.getWorld().getMaxHeight() / 2;
        var loc = chunk.getBlock(8, centerY, 8).getLocation();
        var entities = chunk.getWorld().getNearbyEntities(loc, 8, centerY, 8, ChunkLoadManager::isChunkLoaderEntity);
        return entities.stream().findFirst().map(x -> new ChunkLoader(x.getLocation().add(0, -0.5, 0).getBlock(), x));
    }

    public static boolean isChunkLoader(Block block) {
        if (block == null) return false;
        var loc = block.getLocation().add(0.5, 1, 0.5);
        return !block.getWorld().getNearbyEntities(loc, 0.1, 0.1, 0.1, ChunkLoadManager::isChunkLoaderEntity).isEmpty();
    }

    static boolean isChunkLoaderEntity(Entity entity) {
        return entity.getScoreboardTags().contains(ChunkLoaderTag);
    }

    public static Collection<Chunk> getLoadedChunks() {
        return Collections.unmodifiableCollection(chunks);
    }
}
