package kame.nekotools.world.chunkloader;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemDisplay;
import org.bukkit.util.Consumer;

public class ChunkLoader {

    private final Block block;
    private final Entity entity;

    ChunkLoader(Block block, Entity entity) {
        this.block = block;
        this.entity = entity;
    }

    public void destroy() {
        block.setType(Material.AIR);
        entity.remove();
    }

    public Chunk getChunk() {
        return block.getChunk();
    }

    public static ChunkLoader create(Block block, Consumer<ItemDisplay> function) {
        var loc = block.getLocation().add(0.5, 1, 0.5);
        return new ChunkLoader(block, block.getWorld().spawn(loc, ItemDisplay.class, function));
    }

    public void setForceLoaded(boolean force) {
        if (force && !getChunk().isLoaded()) getChunk().load();
        getChunk().setForceLoaded(force);
    }

    public Location getLocation() {
        return entity.getLocation();
    }

    public World getWorld() {
        return block.getWorld();
    }

}
