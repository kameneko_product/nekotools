package kame.nekotools.world.chunkloader;

import kame.nekotools.NekoTools;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.util.Arrays;

public class ChunkLoaderListener implements Listener {

    public ChunkLoaderListener() {
        ChunkLoadManager.initialize();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onChunkUnload(ChunkUnloadEvent event) {
        if (ChunkLoadManager.isLoaded(event.getChunk())) {
            try {
                event.getChunk().setForceLoaded(true);
            } catch (Exception e) {
                event.getChunk().load();
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockBreak(BlockBreakEvent event) {
        var chunk = event.getBlock().getChunk();
        var loader = ChunkLoadManager.findFromBlock(event.getBlock());
        if (loader.isPresent()) {
            if (!event.getPlayer().hasPermission("nekotools.command.nekotools.chunkloader")) {
                event.getPlayer().sendMessage("§c[ChunkLoader]§r 権限がないためチャンクローダーを破壊出来ません");
                event.setCancelled(true);
            } else {
                event.getPlayer().sendMessage("§a[ChunkLoader]§r チャンクローダーを破壊しました");
                loader.ifPresent(ChunkLoadManager::delete);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getItemInHand().isSimilar(ChunkLoadManager.getItem())) return;
        if (ChunkLoadManager.isLoaded(event.getBlock().getChunk())) {
            event.getPlayer().sendMessage("§c[ChunkLoader]§r 既にこのチャンクはロードされています");
            var chunk = event.getBlock().getChunk();
            var stand = Arrays.stream(chunk.getEntities()).filter(ChunkLoadManager::isChunkLoaderEntity).findAny();
            if (stand.isPresent()) {
                event.setCancelled(true);
                return;
            }
            event.getPlayer().sendMessage("§c[ChunkLoader]§r チャンクローダーが存在しないため設置しました");
        } else {
            event.getPlayer().sendMessage("§a[ChunkLoader]§r チャンクローダーを設置しました");
        }
        var loader = ChunkLoadManager.createNew(event.getBlock());
        var task = Bukkit.getScheduler().runTaskTimer(NekoTools.getInstance(), () -> ChunkLoaderViewer.showEffect(loader), 0, 5);
        Bukkit.getScheduler().runTaskLater(NekoTools.getInstance(), task::cancel, 100);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockChange(EntityChangeBlockEvent event) {
        if (ChunkLoadManager.isChunkLoader(event.getBlock())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockPiston(BlockPistonExtendEvent event) {
        if (event.getBlocks().stream().anyMatch(ChunkLoadManager::isChunkLoader)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockPiston(BlockPistonRetractEvent event) {
        if (event.getBlocks().stream().anyMatch(ChunkLoadManager::isChunkLoader)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockBreak(EntityExplodeEvent event) {
        event.blockList().removeIf(ChunkLoadManager::isChunkLoader);
    }

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    private void onBlockBreak(BlockExplodeEvent event) {
        event.blockList().removeIf(ChunkLoadManager::isChunkLoader);
    }

    @EventHandler
    private void onBlockInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        var optional = ChunkLoadManager.findFromBlock(event.getClickedBlock());
        if (optional.isEmpty()) return;
        var loader = optional.get();
        var task = Bukkit.getScheduler().runTaskTimer(NekoTools.getInstance(), () -> ChunkLoaderViewer.showEffect(loader), 0, 5);
        Bukkit.getScheduler().runTaskLater(NekoTools.getInstance(), task::cancel, 100);
    }

}
